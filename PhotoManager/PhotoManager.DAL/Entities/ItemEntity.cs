﻿using System.ComponentModel.DataAnnotations;
using PhotoManager.DAL.Entities.Base.Implementation;
using System.Collections.Generic;

namespace PhotoManager.DAL.Entities
{
    public class ItemEntity : EntityBase
    {
        [Required]
        public string Name { get; set; }

        public virtual ICollection<ItemTagEntity> Photos { get; set; } = new List<ItemTagEntity>();
    }
}
