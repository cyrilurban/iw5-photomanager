﻿using PhotoManager.DAL.Entities.Tag.Implementation;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;

namespace PhotoManager.DAL.Entities
{
    public class ItemTagEntity : TagEntity
    {
        [ForeignKey("Item")]
        public Guid ItemId { get; set; }
        public virtual ItemEntity Item { get; set; }

        [ForeignKey("Photo")]
        public Guid PhotoId { get; set; }
        public virtual PhotoEntity Photo{ get; set; }

        public override string GetName()
        {
            return this.Item.Name;
        }
    }
}
