﻿using PhotoManager.DAL.Entities.Base.Implementation;
using PhotoManager.DAL.Entities.Tag.Implementation;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PhotoManager.DAL.Entities
{
    public class PhotoEntity : EntityBase
    {
        [Required]
        public string Name { get; set; }

        public byte[] Data { get; set; }
        public string ImagePath { get; set; }

        [Required]
        public string Format { get; set; }

        [Required]
        public int ResolutionX { get; set; }

        [Required]
        public int ResolutionY { get; set; }

        [Column(TypeName = "datetime2")]
        public virtual DateTime Time { get; set; }

        public string Note { get; set; }

        public virtual ICollection<ItemTagEntity> ItemTags { get; set; } = new List<ItemTagEntity>();

        public virtual ICollection<PersonTagEntity> PersonTags { get; set; } = new List<PersonTagEntity>();
 
        public virtual ICollection<AlbumEntity> Albums { get; set; } = new List<AlbumEntity>();
    }
}