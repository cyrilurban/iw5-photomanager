﻿using PhotoManager.DAL.Entities.Base.Implementation;
using System.ComponentModel.DataAnnotations;

namespace PhotoManager.DAL.Entities.Tag.Implementation
{
    public abstract class TagEntity : EntityBase, ITagEntity
    {
        [Required]
        public int PositionX { get; set; }

        [Required]
        public int PositionY { get; set; }

        public abstract string GetName();
    }
}
