﻿
namespace PhotoManager.DAL.Entities
{
    public interface ITagEntity
    {
        int PositionX { get; set; }

        int PositionY { get; set; }

        string GetName();
    }
}