﻿using PhotoManager.DAL.Entities.Base.Implementation;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace PhotoManager.DAL.Entities
{
    public class AlbumEntity : EntityBase
    {
        [Required]
        public string Name { get; set; }

        public virtual ICollection<PhotoEntity> Photos { get; set; } = new List<PhotoEntity>();
    }
}
