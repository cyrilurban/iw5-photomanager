﻿using PhotoManager.DAL.Entities.Base.Interface;
using System;

namespace PhotoManager.DAL.Entities.Base.Implementation
{
    public abstract class EntityBase : IEntity
    {
        public Guid Id { get; set; } = Guid.NewGuid();
    }
}