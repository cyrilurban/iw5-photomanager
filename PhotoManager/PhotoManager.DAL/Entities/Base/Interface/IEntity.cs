﻿using System;

namespace PhotoManager.DAL.Entities.Base.Interface
{
    public interface IEntity
    {
        Guid Id { get; }
    }
}
