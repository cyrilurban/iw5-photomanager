﻿using PhotoManager.DAL.Entities.Base.Implementation;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace PhotoManager.DAL.Entities
{
    public class PersonEntity : EntityBase
    {
        [Required]
        public string Firstname { get; set; }

        [Required]
        public string Surname { get; set; }

        public virtual ICollection<PersonTagEntity> Photos { get; set; } = new List<PersonTagEntity>();
    }
}
