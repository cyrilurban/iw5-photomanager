﻿using PhotoManager.DAL.Entities.Tag.Implementation;
using System;
using System.ComponentModel.DataAnnotations;
namespace PhotoManager.DAL.Entities
{
    public class PersonTagEntity : TagEntity
    {
        [Required]
        public Guid PersonId { get; set; }
        public virtual PersonEntity Person { get; set; }

        [Required]
        public Guid PhotoId { get; set; }
        public virtual PhotoEntity Photo { get; set; }

        public override string GetName()
        {
            return $"{this.Person.Firstname} {this.Person.Surname}";
        }
    }
}
