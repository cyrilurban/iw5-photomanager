﻿using PhotoManager.DAL.Entities;
using System.Data.Entity;
using System;

namespace PhotoManager.DAL
{
    public class PhotoManagerDbContext : DbContext
    {
        public IDbSet<AlbumEntity> Albums { get; set; }
        public IDbSet<PhotoEntity> Photos { get; set; }
        public IDbSet<ItemEntity> Items { get; set; }
        public IDbSet<PersonEntity> People { get; set; }
        public IDbSet<ItemTagEntity> ItemTags { get; set; }
        public IDbSet<PersonTagEntity> PersonTags { get; set; }

        public PhotoManagerDbContext() : base(@"Data Source=(localdb)\MSSQLLocalDB;Initial Catalog=PhotoManagerDB;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=True;ApplicationIntent=ReadWrite;MultiSubnetFailover=False")
        {
        }
        public override int SaveChanges()
        {
            return base.SaveChanges();
        }
    }
}
