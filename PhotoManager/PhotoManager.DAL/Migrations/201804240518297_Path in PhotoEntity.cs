namespace PhotoManager.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class PathinPhotoEntity : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.PhotoEntities", "ImagePath", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.PhotoEntities", "ImagePath");
        }
    }
}
