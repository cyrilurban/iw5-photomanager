namespace PhotoManager.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class inittables : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.AlbumEntities",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Name = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.PhotoEntities",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Name = c.String(nullable: false),
                        Data = c.Binary(),
                        Format = c.String(nullable: false),
                        ResolutionX = c.Int(nullable: false),
                        ResolutionY = c.Int(nullable: false),
                        Time = c.DateTime(nullable: false),
                        Note = c.String(),
                        ItemEntity_Id = c.Guid(),
                        PersonEntity_Id = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.ItemEntities", t => t.ItemEntity_Id)
                .ForeignKey("dbo.PersonEntities", t => t.PersonEntity_Id)
                .Index(t => t.ItemEntity_Id)
                .Index(t => t.PersonEntity_Id);
            
            CreateTable(
                "dbo.ItemTagEntities",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        ItemId = c.Guid(nullable: false),
                        PhotoId = c.Guid(nullable: false),
                        PositionX = c.Int(nullable: false),
                        PositionY = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.ItemEntities", t => t.ItemId, cascadeDelete: true)
                .ForeignKey("dbo.PhotoEntities", t => t.PhotoId, cascadeDelete: true)
                .Index(t => t.ItemId)
                .Index(t => t.PhotoId);
            
            CreateTable(
                "dbo.ItemEntities",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Name = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.PersonTagEntities",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        PersonId = c.Guid(nullable: false),
                        PhotoId = c.Guid(nullable: false),
                        PositionX = c.Int(nullable: false),
                        PositionY = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.PersonEntities", t => t.PersonId, cascadeDelete: true)
                .ForeignKey("dbo.PhotoEntities", t => t.PhotoId, cascadeDelete: true)
                .Index(t => t.PersonId)
                .Index(t => t.PhotoId);
            
            CreateTable(
                "dbo.PersonEntities",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Firstname = c.String(nullable: false),
                        Surname = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.PhotoEntityAlbumEntities",
                c => new
                    {
                        PhotoEntity_Id = c.Guid(nullable: false),
                        AlbumEntity_Id = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => new { t.PhotoEntity_Id, t.AlbumEntity_Id })
                .ForeignKey("dbo.PhotoEntities", t => t.PhotoEntity_Id, cascadeDelete: true)
                .ForeignKey("dbo.AlbumEntities", t => t.AlbumEntity_Id, cascadeDelete: true)
                .Index(t => t.PhotoEntity_Id)
                .Index(t => t.AlbumEntity_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.PersonTagEntities", "PhotoId", "dbo.PhotoEntities");
            DropForeignKey("dbo.PersonTagEntities", "PersonId", "dbo.PersonEntities");
            DropForeignKey("dbo.PhotoEntities", "PersonEntity_Id", "dbo.PersonEntities");
            DropForeignKey("dbo.ItemTagEntities", "PhotoId", "dbo.PhotoEntities");
            DropForeignKey("dbo.ItemTagEntities", "ItemId", "dbo.ItemEntities");
            DropForeignKey("dbo.PhotoEntities", "ItemEntity_Id", "dbo.ItemEntities");
            DropForeignKey("dbo.PhotoEntityAlbumEntities", "AlbumEntity_Id", "dbo.AlbumEntities");
            DropForeignKey("dbo.PhotoEntityAlbumEntities", "PhotoEntity_Id", "dbo.PhotoEntities");
            DropIndex("dbo.PhotoEntityAlbumEntities", new[] { "AlbumEntity_Id" });
            DropIndex("dbo.PhotoEntityAlbumEntities", new[] { "PhotoEntity_Id" });
            DropIndex("dbo.PersonTagEntities", new[] { "PhotoId" });
            DropIndex("dbo.PersonTagEntities", new[] { "PersonId" });
            DropIndex("dbo.ItemTagEntities", new[] { "PhotoId" });
            DropIndex("dbo.ItemTagEntities", new[] { "ItemId" });
            DropIndex("dbo.PhotoEntities", new[] { "PersonEntity_Id" });
            DropIndex("dbo.PhotoEntities", new[] { "ItemEntity_Id" });
            DropTable("dbo.PhotoEntityAlbumEntities");
            DropTable("dbo.PersonEntities");
            DropTable("dbo.PersonTagEntities");
            DropTable("dbo.ItemEntities");
            DropTable("dbo.ItemTagEntities");
            DropTable("dbo.PhotoEntities");
            DropTable("dbo.AlbumEntities");
        }
    }
}
