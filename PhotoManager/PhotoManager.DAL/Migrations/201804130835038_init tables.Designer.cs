// <auto-generated />
namespace PhotoManager.DAL.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.2.0-61023")]
    public sealed partial class inittables : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(inittables));
        
        string IMigrationMetadata.Id
        {
            get { return "201804130835038_init tables"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
