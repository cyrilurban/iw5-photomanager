﻿using PhotoManager.BL.Models;
using PhotoManager.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhotoManager.BL.Messages
{
    public class PersonTagSelectedMessage
    {
        public PersonTagListModel detail { get; set; }
        public PersonTagSelectedMessage(PersonTagListModel personTag)
        {
            detail = personTag;
        }
    }
}
