﻿using System;
using PhotoManager.BL.Models;

namespace PhotoManager.BL.Messages
{
    public class UpdatedItemMessage
    {
        public ItemDetailModel Model { get; set; }
        public UpdatedItemMessage(ItemDetailModel model)
        {
            Model = model;
        }


    }
}