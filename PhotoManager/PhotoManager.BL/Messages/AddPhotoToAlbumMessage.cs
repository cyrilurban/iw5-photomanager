﻿using PhotoManager.BL.Mappers;
using PhotoManager.BL.Models;
using PhotoManager.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhotoManager.BL.Messages
{
    public class AddPhotoToAlbumMessage
    {
        public PhotoListModel photo { get; set; }

        public AddPhotoToAlbumMessage(PhotoListModel photo)
        {
            this.photo = photo;
        }
    }
}
