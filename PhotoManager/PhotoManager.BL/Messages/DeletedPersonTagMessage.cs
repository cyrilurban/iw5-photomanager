﻿using PhotoManager.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhotoManager.BL.Messages
{
    public class DeletedPersonTagMessage
    {
        public Guid personId;
        public DeletedPersonTagMessage(Guid personId)
        {
            this.personId = personId;
        }
    }
}
