﻿using System;

namespace PhotoManager.BL.Messages
{
    public class SelectedAlbumMessage
    {
        public Guid Id { get; set; }
    }
}