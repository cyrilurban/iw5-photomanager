﻿using PhotoManager.BL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhotoManager.BL.Messages
{
    public class UpdatedPersonTagMessage
    {
        public PersonTagDetailModel personTag { get; set; }

        public UpdatedPersonTagMessage(PersonTagDetailModel personTag)
        {
            this.personTag = personTag;
        }
    }
}
