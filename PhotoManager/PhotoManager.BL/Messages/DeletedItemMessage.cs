﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhotoManager.BL.Messages
{
    public class DeletedItemMessage
    {
        public Guid itemId;

        public DeletedItemMessage(Guid itemId)
        {
            this.itemId = itemId;
        }
    }
}
