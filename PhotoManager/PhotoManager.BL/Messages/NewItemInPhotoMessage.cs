﻿using PhotoManager.BL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhotoManager.BL.Messages
{
    public class NewItemInPhotoMessage
    {
        public PhotoDetailModel photo { get; set; }
        public NewItemInPhotoMessage(PhotoDetailModel photo)
        {
            this.photo = photo;
        }
    }
}
