﻿using System;
using PhotoManager.BL.Models;

namespace PhotoManager.BL.Messages
{
    public class UpdatedAlbumMessage
    {
        public AlbumDetailModel Model { get; set; }
        public UpdatedAlbumMessage(AlbumDetailModel model)
        {
            Model = model;
        }


    }
}