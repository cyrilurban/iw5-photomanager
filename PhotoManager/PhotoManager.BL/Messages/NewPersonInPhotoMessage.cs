﻿using PhotoManager.BL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhotoManager.BL.Messages
{
    public class NewPersonInPhotoMessage
    {
        public PhotoDetailModel photo { get; set; }
        public NewPersonInPhotoMessage(PhotoDetailModel photo)
        {
            this.photo = photo;
        }
    }
}
