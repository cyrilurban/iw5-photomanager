﻿using System;

namespace PhotoManager.BL.Messages
{
    public class SelectedPhotoMessage
    {
        public Guid Id { get; set; }
    }
}