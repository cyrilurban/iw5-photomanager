﻿using PhotoManager.BL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhotoManager.BL.Messages
{
    public class UpdatedItemTagMessage
    {
        public ItemTagDetailModel  itemTag {get; set;}

        public UpdatedItemTagMessage(ItemTagDetailModel itemTag)
        {
            this.itemTag = itemTag;
        }
    }
}
