﻿using System;

namespace PhotoManager.BL.Messages
{
    public class SelectedItemMessage
    {
        public Guid Id { get; set; }
    }
}