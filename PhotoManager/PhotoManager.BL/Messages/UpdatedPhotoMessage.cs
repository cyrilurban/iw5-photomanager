﻿using System;
using PhotoManager.BL.Models;

namespace PhotoManager.BL.Messages
{
    public class UpdatedPhotoMessage
    {
        public PhotoDetailModel Model { get; set; }
        public UpdatedPhotoMessage(PhotoDetailModel model)
        {
            Model = model;
        }


    }
}