﻿using System;

namespace PhotoManager.BL.Messages
{
    public class SelectedPersonMessage
    {
        public Guid Id { get; set; }
    }
}