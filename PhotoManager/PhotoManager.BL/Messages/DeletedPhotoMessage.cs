﻿using System;

namespace PhotoManager.BL.Messages
{
    public class DeletedPhotoMessage
    {
        public Guid photoId;

        public DeletedPhotoMessage(Guid photoId)
        {
            this.photoId = photoId;
        }
    }
}