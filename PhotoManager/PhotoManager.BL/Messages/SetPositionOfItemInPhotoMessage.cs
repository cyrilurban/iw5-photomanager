﻿using PhotoManager.BL.Models;
using PhotoManager.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhotoManager.BL.Messages
{
    
    public class SetPositionOfItemInPhotoMessage
    {
        public ItemListModel item { get; set; }
        public PhotoDetailModel photo { get; set; }
        public SetPositionOfItemInPhotoMessage(ItemListModel item, PhotoDetailModel photo)
        {
            this.item = item;
            this.photo = photo;
        }

    }
}
