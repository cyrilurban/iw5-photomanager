﻿using System;
using PhotoManager.BL.Models;

namespace PhotoManager.BL.Messages
{
    public class UpdatedPersonMessage
    {
        public PersonDetailModel Model { get; set; }
        public UpdatedPersonMessage(PersonDetailModel model)
        {
            Model = model;
        }


    }
}