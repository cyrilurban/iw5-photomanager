﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhotoManager.BL.Messages
{
    public class DeletedPersonMessage
    {
        public Guid personId;

        public DeletedPersonMessage(Guid personId)
        {
            this.personId = personId;
        }
    }
}
