﻿using PhotoManager.BL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhotoManager.BL.Messages
{
    public class SetPositionOfPersonInPhotoMessage
    {
        public PersonListModel person { get; set; }
        public PhotoDetailModel photo { get; set; }
        public SetPositionOfPersonInPhotoMessage(PersonListModel person, PhotoDetailModel photo)
        {
            this.person = person;
            this.photo = photo;
        }
    }
}
