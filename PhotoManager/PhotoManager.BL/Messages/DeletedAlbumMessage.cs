﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhotoManager.BL.Messages
{
    public class DeletedAlbumMessage
    {
        public Guid albumId;

        public DeletedAlbumMessage(Guid albumId)
        {
            this.albumId = albumId;
        }
    }
}
