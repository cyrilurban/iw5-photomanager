﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhotoManager.BL.Messages
{
    public class DeletedItemTagMessage
    {
        public Guid itemTagId;
        public DeletedItemTagMessage(Guid itemTagId)
        {
            this.itemTagId = itemTagId;
        }
    }
}
