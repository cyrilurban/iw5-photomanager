﻿using PhotoManager.BL.Models;
using PhotoManager.BL.Repositories;
using PhotoManager.DAL.Entities;
using System;
using System.Collections.Generic;

namespace PhotoManager.BL.Mappers
{
    public class PhotoMapper: IMapper<PhotoEntity, PhotoListModel, PhotoDetailModel>
    {
        private readonly HelperRepository helperRepository= new HelperRepository();
        public PhotoListModel MapEntityToListModel(PhotoEntity entity)
        {
            return new PhotoListModel
            {
                Id = entity.Id,
                Format = entity.Format,
                Name = entity.Name,
                Time = entity.Time,
                ResolutionX = entity.ResolutionX,
                ResolutionY = entity.ResolutionY
            };
        }

        public PhotoDetailModel MapEntityToDetailModel(PhotoEntity entity)
        {
            List<PersonTagListModel> people = new List<PersonTagListModel>();
            foreach(var p in entity.PersonTags)
            {
                people.Add(new PersonTagListModel
                {
                    Id = p.Id,
                    Firstname = p.Person.Firstname,
                    Surname = p.Person.Surname,
                    Photoname = p.Photo.Name,
                    PositionY = p.PositionY,
                    PositionX = p.PositionX
                });
            }

            List<ItemTagListModel> items = new List<ItemTagListModel>();
            foreach (var p in entity.ItemTags)
            {
                items.Add(new ItemTagListModel
                {
                    Id = p.Id,
                    Name = p.Item.Name,
                    Photoname = p.Photo.Name,
                    PositionY = p.PositionY,
                    PositionX = p.PositionX
                });
            }

            List<AlbumListModel> albums = new List<AlbumListModel>();
            foreach (var p in entity.Albums)
            {
                albums.Add(new AlbumListModel
                {
                    Id = p.Id,
                    Name = p.Name                   
                });
            }
            return new PhotoDetailModel
            {
                Id = entity.Id,
                Name = entity.Name,
                ResolutionX = entity.ResolutionX,
                ResolutionY = entity.ResolutionY,
                ImagePath = entity.ImagePath,
                Note = entity.Note,
                Format = entity.Format,
                Data = entity.Data,
                Time = entity.Time,
                PersonTags = people,
                ItemTags = items,
                Albums = albums
            };
        }

        public PhotoEntity MapDetailModelToEntity(PhotoDetailModel detail)
        {
            List<PersonTagEntity> people = new List<PersonTagEntity>();
            if(detail.PersonTags != null)
            {
                foreach (var p in detail.PersonTags)
                {
                    people.Add(helperRepository.GetPersonTagById(p.Id));
                }
            }
            

            List<ItemTagEntity> items = new List<ItemTagEntity>();
            if(detail.ItemTags != null)
            {
                foreach (var p in detail.ItemTags)
                {
                    items.Add(helperRepository.GetItemTagById(p.Id));
                }
            }

            List<AlbumEntity> albums = new List<AlbumEntity>();
            if (detail.Albums != null)
            {
                foreach (var p in detail.Albums)
                {
                    albums.Add(helperRepository.GetAlbumById(p.Id));
                }
            }

            return new PhotoEntity()
            {
                Albums = albums,
                Data = detail.Data,
                Format = detail.Format,
                Id = detail.Id,
                ImagePath = detail.ImagePath,
                ItemTags = items,
                Name = detail.Name,
                Note = detail.Note,
                PersonTags = people,
                ResolutionX = detail.ResolutionX,
                ResolutionY = detail.ResolutionY,
                Time = detail.Time
            };
        }

        public PhotoListModel MapDetailModelToListModel(PhotoDetailModel detail)
        {
            return new PhotoListModel
            {
                Id = detail.Id,
                Name = detail.Name,
                Format = detail.Format,
                ResolutionX = detail.ResolutionX,
                ResolutionY = detail.ResolutionY,
                Time = detail.Time
            };
        }
    }
}
