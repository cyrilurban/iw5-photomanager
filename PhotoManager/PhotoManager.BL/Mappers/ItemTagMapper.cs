﻿using System;
using PhotoManager.BL.Models;
using PhotoManager.DAL.Entities;
using PhotoManager.BL.Repositories;

namespace PhotoManager.BL.Mappers
{
    public class ItemTagMapper : IMapper<ItemTagEntity, ItemTagListModel, ItemTagDetailModel>
    {
        HelperRepository helperRepository = new HelperRepository();
        public ItemTagEntity MapDetailModelToEntity(ItemTagDetailModel detailModel)
        {
            ItemEntity itemEntity = helperRepository.GetItemById(detailModel.Item.Id);
            PhotoEntity photoEntity = helperRepository.GetPhotoById(detailModel.Photo.Id);
            return new ItemTagEntity()
            {
                Id = detailModel.Id,
                Item = itemEntity,
                //ItemId = detailModel.ItemId,
                Photo = photoEntity,
               // PhotoId = detailModel.PhotoId,
                PositionX = detailModel.PositionX,
                PositionY = detailModel.PositionY
            };
        }

        public ItemTagDetailModel MapEntityToDetailModel(ItemTagEntity entity)
        {
            ItemDetailModel itemDetail = new ItemDetailModel
            {
                Id = entity.Item.Id,
                Name = entity.Item.Name
            };

            PhotoDetailModel photoDetail = new PhotoDetailModel
            {
                Id = entity.Photo.Id,
                Name = entity.Photo.Name,
                ImagePath = entity.Photo.ImagePath
            };
            return new ItemTagDetailModel()
            {
                Id = entity.Id,
                Item = itemDetail,
              //  ItemId = entity.ItemId,
                Photo = photoDetail,
              //  PhotoId = entity.PhotoId,
                PositionX = entity.PositionX,
                PositionY = entity.PositionY
            };
        }

        public ItemTagListModel MapEntityToListModel(ItemTagEntity entity)
        {
            return new ItemTagListModel
            {
                Id = entity.Id,
                Name = entity.Item.Name,
                Photoname = entity.Photo.Name,
                PositionX = entity.PositionX,
                PositionY = entity.PositionY
            };
        }
    }
}
