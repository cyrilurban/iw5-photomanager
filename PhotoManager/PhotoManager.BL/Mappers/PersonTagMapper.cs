﻿using PhotoManager.BL.Models;
using PhotoManager.BL.Repositories;
using PhotoManager.DAL.Entities;

namespace PhotoManager.BL.Mappers
{
    public class PersonTagMapper : IMapper<PersonTagEntity, PersonTagListModel, PersonTagDetailModel>
    {
        HelperRepository helperRepository = new HelperRepository();
        public PersonTagEntity MapDetailModelToEntity(PersonTagDetailModel detailModel)
        {
            PersonEntity person = helperRepository.GetPersonById(detailModel.Person.Id);
            PhotoEntity photo = helperRepository.GetPhotoById(detailModel.Photo.Id);
            return new PersonTagEntity()
            {
                Id = detailModel.Id,
                Person = person,
                PersonId = detailModel.PersonId,
                Photo = photo,
                PhotoId = detailModel.PhotoId,
                PositionX = detailModel.PositionX,
                PositionY = detailModel.PositionY
            };
        }

        public PersonTagDetailModel MapEntityToDetailModel(PersonTagEntity entity)
        {
            PersonDetailModel personDetail = new PersonDetailModel
            {
                Id = entity.Person.Id,
                Firstname = entity.Person.Firstname,
                Surname = entity.Person.Surname
            };

            PhotoDetailModel photoDetail = new PhotoDetailModel
            {
                Id = entity.Photo.Id,
                Name = entity.Photo.Name,
                ImagePath = entity.Photo.ImagePath
            };

            return new PersonTagDetailModel()
            {
                Id = entity.Id,
                Person = personDetail,
                PersonId = entity.PersonId,
                Photo = photoDetail,
                PhotoId = entity.PhotoId,
                PositionX = entity.PositionX,
                PositionY = entity.PositionY
            };
        }

        public PersonTagListModel MapEntityToListModel(PersonTagEntity entity)
        {
            return new PersonTagListModel()
            {
                Id = entity.Id,
                Firstname = entity.Person.Firstname,
                Surname = entity.Person.Surname,
                Photoname = entity.Photo.Name,
                PositionY = entity.PositionY,
                PositionX = entity.PositionX
            };
        }
    }
}
