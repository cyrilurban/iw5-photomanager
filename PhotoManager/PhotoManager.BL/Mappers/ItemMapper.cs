﻿using PhotoManager.BL.Models;
using PhotoManager.BL.Repositories;
using PhotoManager.DAL.Entities;
using System.Collections.Generic;

namespace PhotoManager.BL.Mappers
{
    public class ItemMapper : IMapper<ItemEntity, ItemListModel, ItemDetailModel>
    {
        HelperRepository helperRepository = new HelperRepository();
        public ItemListModel MapEntityToListModel(ItemEntity entity)
        {
            return new ItemListModel { Id = entity.Id, Name = entity.Name};
        }

        public ItemDetailModel MapEntityToDetailModel(ItemEntity entity)
        {
            List<ItemTagListModel> photos = new List<ItemTagListModel>();
            foreach(var p in entity.Photos)
            {
                photos.Add(new ItemTagListModel
                {
                    Id = p.Id,
                    Name = p.Item.Name,
                    Photoname = p.Photo.Name,
                    PositionX = p.PositionX,
                    PositionY = p.PositionY

                });
            }
            return new ItemDetailModel {
                Id = entity.Id,
                Name = entity.Name,
                Photos = photos };
        }

        public ItemEntity MapDetailModelToEntity(ItemDetailModel detail)
        {
            List<ItemTagEntity> photos = new List<ItemTagEntity>();
            if(detail.Photos != null)
            {
                foreach (var p in detail.Photos)
                {
                    photos.Add(helperRepository.GetItemTagById(p.Id));
                }
            }
            
            return new ItemEntity() {
                Id = detail.Id,
                Name = detail.Name,
                Photos = photos };
        }

        public ItemListModel MapDetailModelToListModel(ItemDetailModel detail)
        {
            return new ItemListModel
            {
                Id = detail.Id,
                Name = detail.Name,
            };
        }
    }
}
