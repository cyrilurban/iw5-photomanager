﻿namespace PhotoManager.BL.Mappers
{
    interface IMapper<Entity, ListModel, DetailModel>
    {
        ListModel MapEntityToListModel(Entity entity);

        DetailModel MapEntityToDetailModel(Entity entity);

        Entity MapDetailModelToEntity(DetailModel detailModel);
    }
}
