﻿using PhotoManager.BL.Models;
using PhotoManager.BL.Repositories;
using PhotoManager.DAL.Entities;
using System.Collections.Generic;

namespace PhotoManager.BL.Mappers
{
    public class AlbumMapper : IMapper<AlbumEntity, AlbumListModel, AlbumDetailModel>
    {
        HelperRepository helperRepository = new HelperRepository();
        public AlbumDetailModel MapEntityToDetailModel(AlbumEntity entity)
        {
            List<PhotoListModel> photos = new List<PhotoListModel>();

            if(entity.Photos != null)
            {
                foreach (var p in entity.Photos)
                {
                    photos.Add(new PhotoListModel
                    {
                        Id = p.Id,
                        Name = p.Name
                    });
                }
            }
            return new AlbumDetailModel {
                Id = entity.Id,
                Name = entity.Name,
                Photos = photos };
        }

        public AlbumListModel MapEntityToListModel(AlbumEntity entity)
        {
            return new AlbumListModel { Id = entity.Id, Name = entity.Name, NumberOfPhotos = entity.Photos.Count };
        }

        public AlbumEntity MapDetailModelToEntity(AlbumDetailModel detailModel)
        {
            List<PhotoEntity> photos = new List<PhotoEntity>();
            foreach(var p in detailModel.Photos)
            {
                photos.Add(helperRepository.GetPhotoById(p.Id));
            }
            return new AlbumEntity {
                Id = detailModel.Id,
                Name = detailModel.Name,
                Photos = photos };
        }

        public AlbumListModel MapDetailModelToListModel(AlbumDetailModel detailModel)
        {
            return new AlbumListModel
            {
                Id = detailModel.Id,
                Name = detailModel.Name,
                NumberOfPhotos = detailModel.Photos.Count
            };
        }
    }
}
