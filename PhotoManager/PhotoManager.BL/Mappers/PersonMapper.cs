﻿using System;
using PhotoManager.BL.Models;
using PhotoManager.DAL.Entities;
using PhotoManager.BL.Repositories;
using System.Collections.Generic;

namespace PhotoManager.BL.Mappers
{
    public class PersonMapper : IMapper<PersonEntity, PersonListModel, PersonDetailModel>
    {
        HelperRepository helperRepository = new HelperRepository();
        public PersonDetailModel MapEntityToDetailModel(PersonEntity entity)
        {
            List<PersonTagListModel> photos = new List<PersonTagListModel>();
            foreach (var p in entity.Photos)
            {
                photos.Add(new PersonTagListModel
                {
                    Id = p.Id,
                    Firstname = p.Person.Firstname,
                    Surname = p.Person.Surname,
                    Photoname = p.Photo.Name,
                    PositionX = p.PositionX,
                    PositionY = p.PositionY

                });
            }
            return new PersonDetailModel
            {
                Id = entity.Id,
                Firstname = entity.Firstname,
                Surname = entity.Surname,
                Photos = photos
            };
        }

        public PersonListModel MapEntityToListModel(PersonEntity entity)
        {
            return new PersonListModel { Id = entity.Id, Name = $"{entity.Firstname} {entity.Surname}" };
        }

        public PersonEntity MapDetailModelToEntity(PersonDetailModel detail)
        {
            List<PersonTagEntity> photos = new List<PersonTagEntity>();
            if (detail.Photos != null)
            {
                foreach (var p in detail.Photos)
                {
                    photos.Add(helperRepository.GetPersonTagById(p.Id));
                }
            }
            return new PersonEntity() {
                Id = detail.Id,
                Firstname = detail.Firstname,
                Surname = detail.Surname,
                Photos = photos };
        }

        public PersonListModel MapDetailModelToListModel(PersonDetailModel detail)
        {
            return new PersonListModel
            {
                Id = detail.Id,
                Name = $"{detail.Firstname} {detail.Surname}"
            };
        }
    }
}
