﻿using System;
using System.Collections.Generic;
using System.Linq;
using PhotoManager.BL.Mappers;
using PhotoManager.BL.Models;
using PhotoManager.DAL.Entities;
using PhotoManager.DAL;

namespace PhotoManager.BL.Repositories
{
    public class ItemTagRepository : IRepository<ItemTagDetailModel, ItemTagListModel>
    {
        private ItemTagMapper itemTagMapper = new ItemTagMapper();

        public List<ItemTagListModel> GetAll()
        {
            using (var context = new PhotoManagerDbContext())
            {
                return context.ItemTags.Select(itemTagMapper.MapEntityToListModel).ToList();
            }
        }

        public ItemTagDetailModel GetById(Guid id)
        {
            using (var context = new PhotoManagerDbContext())
            {
                var item = context.ItemTags.FirstOrDefault(i => i.Id == id);

                if (item == null)
                {
                    return null;
                }

                return itemTagMapper.MapEntityToDetailModel(item);
            }
        }

        public ItemTagDetailModel Insert(ItemTagDetailModel detail)
        {
            using (var context = new PhotoManagerDbContext())
            {
                var entity = itemTagMapper.MapDetailModelToEntity(detail);
                entity.Id = Guid.NewGuid();

                var item = context.Items.SingleOrDefault(i => i.Id == detail.Item.Id);
                var photo = context.Photos.SingleOrDefault(p => p.Id == detail.Photo.Id);
                var itemTag = new ItemTagEntity
                {
                    Item = item,
                    Photo = photo,
                    PositionX = detail.PositionX,
                    PositionY = detail.PositionY
                };
                
                context.ItemTags.Add(itemTag);

                //update photo in ItemEntity
                
              //  item.Photos.Add(detail.Photo);

                context.SaveChanges();

                return itemTagMapper.MapEntityToDetailModel(itemTag);
            }
        }

        public void Remove(Guid id)
        {
            using (var context = new PhotoManagerDbContext())
            {
                var entity = new ItemTagEntity() { Id = id };
                context.ItemTags.Attach(entity);

                context.ItemTags.Remove(entity);
                context.SaveChanges();
            }
        }

        public ItemTagDetailModel Update(ItemTagDetailModel detail)
        {
            using (var context = new PhotoManagerDbContext())
            {
                var entity = context
                    .ItemTags
                    .First(a => a.Id == detail.Id);

             //   entity.ItemId = detail.ItemId;
             //   entity.PhotoId = detail.PhotoId;
                entity.PositionX = detail.PositionX;
                entity.PositionY = detail.PositionY;

                context.SaveChanges();
                return itemTagMapper.MapEntityToDetailModel(entity);
            }
        }
    }
}
