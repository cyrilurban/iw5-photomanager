﻿using PhotoManager.DAL;
using PhotoManager.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhotoManager.BL.Repositories
{
    public class HelperRepository
    {
        internal ItemTagEntity GetItemTagById(Guid id)
        {
            using (var context = new PhotoManagerDbContext())
            {
                return context.ItemTags.SingleOrDefault(p => p.Id == id);
            }
        }

        internal PersonTagEntity GetPersonTagById(Guid id)
        {
            using (var context = new PhotoManagerDbContext())
            {
                return context.PersonTags.SingleOrDefault(p => p.Id == id);
            }
        }

        internal PhotoEntity GetPhotoById(Guid id)
        {
            using (var context = new PhotoManagerDbContext())
            {
                return context.Photos.SingleOrDefault(p => p.Id == id);
            }

        }

        internal ItemEntity GetItemById(Guid id)
        {
            using (var context = new PhotoManagerDbContext())
            {
                return context.Items.SingleOrDefault(i => i.Id == id);
            }
        }

        internal PersonEntity GetPersonById(Guid id)
        {
            using (var context = new PhotoManagerDbContext())
            {
                return context.People.SingleOrDefault(p => p.Id == id);
            }
        }

        internal AlbumEntity GetAlbumById(Guid id)
        {
            using (var context = new PhotoManagerDbContext())
            {
                return context.Albums.SingleOrDefault(p => p.Id == id);
            }
        }
    }
}
