﻿using PhotoManager.BL.Mappers;
using PhotoManager.BL.Models;
using PhotoManager.DAL.Entities;
using PhotoManager.DAL;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace PhotoManager.BL.Repositories
{
    public class PersonRepository : IRepository<PersonDetailModel, PersonListModel>
    {
        private readonly PersonMapper personMapper = new PersonMapper();
        private readonly HelperRepository helperRepository= new HelperRepository();
        private void ValidInputThrowEx(string validArgument)
        {
            if (validArgument == null)
            {
                throw new ArgumentNullException();
            }
            if (string.IsNullOrWhiteSpace(validArgument))
            {
                throw new ArgumentException();
            }
        }
        public List<PersonListModel> GetAll()
        {
            using (var context = new PhotoManagerDbContext())
            {
                return context.People
                    .Select(personMapper.MapEntityToListModel)
                    .ToList();
            }
        }

        public PersonDetailModel GetById(Guid personId)
        {
            using (var context = new PhotoManagerDbContext())
            {
                var photo = context.People
                    .Include(p => p.Photos.Select(p2 => p2.Photo))
                    .FirstOrDefault(person => person.Id == personId);

                if (photo == null)
                {
                    return null;
                }

                return personMapper.MapEntityToDetailModel(photo);
            }
        }

        public List<PersonListModel> GetByName(string name)
        {
            ValidInputThrowEx(name);

            using (var context = new PhotoManagerDbContext())
            {
                return context
                    .People
                    .Where(p => p.Firstname == name)
                    .Concat(context.People.Where(p => p.Surname == name))
                    .Select(personMapper.MapEntityToListModel)
                    .ToList();
            }
        }

        public List<PersonListModel> GetByBothNames(string firstname, string surname)
        {
            using (var context = new PhotoManagerDbContext())
            {
                return context
                    .People
                    .Where(p => p.Firstname == firstname && p.Surname == surname)
                    .Select(personMapper.MapEntityToListModel)
                    .ToList();
            }
        }

        public PersonDetailModel Insert(PersonDetailModel detail)
        {
            using (var context = new PhotoManagerDbContext())
            {
                var entity = personMapper.MapDetailModelToEntity(detail);
                entity.Id = Guid.NewGuid();

                context.People.Add(entity);
                context.SaveChanges();

                return personMapper.MapEntityToDetailModel(entity);
            }
        }

        public void Remove(Guid id)
        {
            using (var context = new PhotoManagerDbContext())
            {
                var entity = new PersonEntity() { Id = id };
                context.People.Attach(entity);

                context.People.Remove(entity);
                context.SaveChanges();
            }
        }

        public void Update(PersonDetailModel detail)
        {
            using (var context = new PhotoManagerDbContext())
            {
                var entity = context.People.First(a => a.Id == detail.Id);
                List<PersonTagEntity> photos = new List<PersonTagEntity>();
                foreach (var i in detail.Photos)
                {
                    var personTag = helperRepository.GetPersonTagById(i.Id);
                    context.PersonTags.Attach(personTag);
                    photos.Add(personTag);
                }
                entity.Firstname = detail.Firstname;
                entity.Surname = detail.Surname;
                entity.Photos = photos;

                context.SaveChanges();
            }
        }

        
    }
}
