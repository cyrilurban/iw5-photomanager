﻿using PhotoManager.BL.Mappers;
using PhotoManager.BL.Models;
using PhotoManager.DAL.Entities;
using PhotoManager.DAL;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace PhotoManager.BL.Repositories
{
    public class ItemRepository : IRepository<ItemDetailModel, ItemListModel>
    {
        private readonly ItemMapper itemMapper = new ItemMapper();
        private readonly HelperRepository helperRepository = new HelperRepository();
        private void ValidInputThrowEx(string validArgument)
        {
            if (validArgument == null)
            {
                throw new ArgumentNullException();
            }
            if (string.IsNullOrWhiteSpace(validArgument))
            {
                throw new ArgumentException();
            }
        }

        public List<ItemListModel> GetAll()
        {
            using (var context = new PhotoManagerDbContext())
            {
                return context.Items.Select(itemMapper.MapEntityToListModel).ToList();
            }
        }

        public ItemDetailModel GetById(Guid id)
        {
            using (var context = new PhotoManagerDbContext())
            {
                var item = context
                    .Items
                    .Include(p => p.Photos.Select(p2 => p2.Photo))
                    .FirstOrDefault(i => i.Id == id);

                if (item == null)
                {
                    return null;
                }

                return itemMapper.MapEntityToDetailModel(item);
            }
        }

        public List<ItemListModel> GetByName(string itemName)
        {
            ValidInputThrowEx(itemName);

            using (var context = new PhotoManagerDbContext())
            {
                return context
                    .Items
                    .Where(i => i.Name == itemName)
                    .Select(itemMapper.MapEntityToListModel)
                    .ToList();
            }
        }

        public ItemDetailModel Insert(ItemDetailModel detail)
        {
            using (var context = new PhotoManagerDbContext())
            {
                var entity = itemMapper.MapDetailModelToEntity(detail);
                entity.Id = Guid.NewGuid();

                context.Items.Add(entity);
                context.SaveChanges();

                return itemMapper.MapEntityToDetailModel(entity);
            }
        }

        public void Remove(Guid id)
        {
            using (var context = new PhotoManagerDbContext())
            {
                var entity = new ItemEntity() { Id = id };
                context.Items.Attach(entity);

                context.Items.Remove(entity);
                context.SaveChanges();
            }
        }

        public void Update(ItemDetailModel detail)
        {
            using (var context = new PhotoManagerDbContext())
            {
                var entity = context.Items.First(a => a.Id == detail.Id);
                List<ItemTagEntity> photos = new List<ItemTagEntity>();
                foreach(var i in detail.Photos)
                {
                    photos.Add(helperRepository.GetItemTagById(i.Id));   
                }
                entity.Name = detail.Name;
                entity.Photos = photos;

                context.SaveChanges();
            }
        }

        
    }
}
