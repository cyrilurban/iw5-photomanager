﻿using PhotoManager.BL.Mappers;
using PhotoManager.BL.Models;
using PhotoManager.DAL.Entities;
using PhotoManager.DAL;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace PhotoManager.BL.Repositories
{
    public class AlbumRepository : IRepository<AlbumDetailModel, AlbumListModel>
    {
        private readonly PhotoRepository photoRepository = new PhotoRepository();
        private readonly AlbumMapper albumMapper = new AlbumMapper();
        private readonly PhotoMapper photoMapper = new PhotoMapper();

        private void ValidInputThrowEx(string validArgument)
        {
            if (validArgument == null)
            {
                throw new ArgumentNullException();
            }
            if (string.IsNullOrWhiteSpace(validArgument))
            {
                throw new ArgumentException();
            }
        }

        private void ValidInputGuidThrowEx(Guid validArgument)
        {
            if (validArgument == null)
            {
                throw new ArgumentNullException();
            }
        }

        public AlbumDetailModel Insert(AlbumDetailModel detail)
        {
            using (var context = new PhotoManagerDbContext())
            {
                if (detail.Photos == null)
                {
                    detail.Photos = new List<PhotoListModel>();
                }

                var entity = albumMapper.MapDetailModelToEntity(detail);
                entity.Id = Guid.NewGuid();

                context.Albums.Add(entity);
                context.SaveChanges();

                return albumMapper.MapEntityToDetailModel(entity);
            }
        }

        public AlbumDetailModel GetByName(string albumName)
        {
            ValidInputThrowEx(albumName);

            using (var context = new PhotoManagerDbContext())
            {
                var albumInDB = context
                    .Albums
                    .Include(a => a.Photos)
                    .FirstOrDefault(a => a.Name == albumName);
                return albumMapper.MapEntityToDetailModel(albumInDB);
            }
        }

        public AlbumDetailModel GetById(Guid albumId)
        {
            ValidInputGuidThrowEx(albumId);

            using (var context = new PhotoManagerDbContext())
            {
                var albumInDB = context.Albums
                    .Include(a => a.Photos)
                    .FirstOrDefault(a => a.Id == albumId);

                if (albumInDB == null)
                {
                    return null;
                }

                return albumMapper.MapEntityToDetailModel(albumInDB);
            }
        }

        public List<AlbumListModel> GetAll()
        {
            using (var context = new PhotoManagerDbContext())
            {
                return context.Albums
                    .Include(a => a.Photos)
                    .Select(albumMapper.MapEntityToListModel)
                    .ToList();
            }
        }

        public AlbumDetailModel Update(AlbumDetailModel detail)
        {
            using (var context = new PhotoManagerDbContext())
            {
                var entity = context.Albums
                    .Include(p => p.Photos)
                    .First(a => a.Id == detail.Id);
                context.Albums.Attach(entity);
                var photoList = new List<PhotoEntity>();
                foreach(var photo in detail.Photos)
                {
                        var photo2 = context.Photos.SingleOrDefault(p => p.Id == photo.Id);
                        context.Photos.Attach(photo2);
                        photoList.Add(photo2);
                    
                }

                entity.Photos = photoList;
                entity.Name = detail.Name;
                context.SaveChanges();
                return albumMapper.MapEntityToDetailModel(entity);
            }
        }

        public void Remove(Guid id)
        {
            ValidInputGuidThrowEx(id);

            using (var context = new PhotoManagerDbContext())
            {
                var entity = new AlbumEntity() { Id = id };
                context.Albums.Attach(entity);

                context.Albums.Remove(entity);
                context.SaveChanges();
            }
        }
    }
}
