﻿using PhotoManager.BL.Mappers;
using PhotoManager.BL.Models;
using PhotoManager.DAL.Entities;
using PhotoManager.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;

namespace PhotoManager.BL.Repositories
{
    public class PhotoRepository : IRepository<PhotoDetailModel, PhotoListModel>
    {
        private readonly PhotoMapper photoMapper = new PhotoMapper();

        public List<PhotoListModel> OrderPhotosByDate()
        {
            List<PhotoListModel> photosOrderedByDate = new List<PhotoListModel>();
            using (var context = new PhotoManagerDbContext())
            {
                var photoList = context
                                   .Photos
                                   .OrderBy(p => p.Time)
                                   .ToList();
                foreach (var p in photoList)
                {
                    photosOrderedByDate.Add(photoMapper.MapEntityToListModel(p));
                }
            }
            return photosOrderedByDate;
        }

        public List<PhotoListModel> OrderPhotosByName()
        {
            List<PhotoListModel> photosOrderedByName = new List<PhotoListModel>();
            using (var context = new PhotoManagerDbContext())
            {
                var photoList = context
                    .Photos
                    .OrderBy(p => p.Name)
                    .ToList();
                foreach (var p in photoList)
                {
                    photosOrderedByName.Add(photoMapper.MapEntityToListModel(p));
                }
            }
            return photosOrderedByName;

        }

        // TODO time greater than/ less than?
        public List<PhotoListModel> FilterByTime(DateTime time)
        {
            List<PhotoListModel> photosFilteredByTime = new List<PhotoListModel>();
            using (var context = new PhotoManagerDbContext())
            {
                var photoList = context
                    .Photos
                    .Where(p => p.Time == time)
                    .ToList();
                foreach (var p in photoList)
                {
                    photosFilteredByTime.Add(photoMapper.MapEntityToListModel(p));
                }
            }
            return photosFilteredByTime;
        }

        public List<PhotoListModel> FilterByFormat(string format)
        {
            List<PhotoListModel> photosFilteredByFormat = new List<PhotoListModel>();
            using (var context = new PhotoManagerDbContext())
            {
                var photoList = context
                    .Photos
                    .Where(p => p.Format == format)
                    .ToList();
                foreach (var p in photoList)
                {
                    photosFilteredByFormat.Add(photoMapper.MapEntityToListModel(p));
                }
            }
            return photosFilteredByFormat;
        }

        // only photos with greater resolution
        public List<PhotoListModel> FilterByResolution(int resolutionX, int resolutionY)
        {
            List<PhotoListModel> photosFilteredByResolution = new List<PhotoListModel>();
            using (var context = new PhotoManagerDbContext())
            {
                var photoList = context
                    .Photos
                    .Where(p => p.ResolutionX >= resolutionX && p.ResolutionY >= resolutionY)
                    .ToList();
                foreach (var p in photoList)
                {
                    photosFilteredByResolution.Add(photoMapper.MapEntityToListModel(p));
                }
            }
            return photosFilteredByResolution;
        }


        public PhotoDetailModel GetById(Guid photoId)
        {
            using (var context = new PhotoManagerDbContext())
            {
                var photo = context.Photos
                    .Include(p => p.ItemTags.Select(i => i.Item))
                    .Include(p => p.PersonTags.Select(p2 => p2.Person))
                    .FirstOrDefault(p => p.Id == photoId);

                if (photo == null)
                {
                    return null;
                }

                return photoMapper.MapEntityToDetailModel(photo);
            }
        }

        public List<PhotoListModel> GetAll()
        {
            using (var context = new PhotoManagerDbContext())
            {
                return context.Photos.Select(photoMapper.MapEntityToListModel).ToList();
            }
        }

        public List<PhotoListModel> GetByName(string photoName)
        {
            using (var context = new PhotoManagerDbContext())
            {
                var photos = context
                    .Photos
                    .Where(p => p.Name == photoName)
                    .Select(photoMapper.MapEntityToListModel)
                    .ToList();
                return photos;
            }
        }

        public PhotoDetailModel Insert(PhotoDetailModel detail)
        {
            using (var context = new PhotoManagerDbContext())
            {
                var entity = photoMapper.MapDetailModelToEntity(detail);
                entity.Id = Guid.NewGuid();

                context.Photos.Add(entity);
                context.SaveChanges();

                return photoMapper.MapEntityToDetailModel(entity);
            }
        }

        public PhotoDetailModel Update(PhotoDetailModel detail)
        {
            using (var context = new PhotoManagerDbContext())
            {
                var entity = context.Photos
                    .Include(p => p.ItemTags)
                    .Include(p => p.PersonTags)
                    .First(a => a.Id == detail.Id);
                context.Photos.Attach(entity);

                var itemTagList = new List<ItemTagEntity>();
                foreach(var itemTag in detail.ItemTags)
                {
                    var itemTag2 = context.ItemTags.SingleOrDefault(p => p.Id == itemTag.Id);
                    context.ItemTags.Attach(itemTag2);
                    itemTagList.Add(itemTag2);
                }

                var personTagList = new List<PersonTagEntity>();
                foreach (var personTag in detail.PersonTags)
                {
                    var personTag2 = context.PersonTags.SingleOrDefault(p => p.Id == personTag.Id);
                    context.PersonTags.Attach(personTag2);
                    personTagList.Add(personTag2);
                }

                entity.ItemTags = itemTagList;
                entity.PersonTags = personTagList;

                // entity.Albums = detail.Albums;
                entity.Data = detail.Data;
                entity.Format = detail.Format;
                entity.Id = detail.Id;
                entity.ImagePath = detail.ImagePath;
              //  entity.ItemTags = detail.ItemTags;
                entity.Name = detail.Name;
                entity.Note = detail.Note;
              //  entity.PersonTags = detail.PersonTags;
                entity.ResolutionX = detail.ResolutionX;
                entity.ResolutionY = detail.ResolutionY;
                entity.Time = detail.Time;

                context.SaveChanges();
                return photoMapper.MapEntityToDetailModel(entity);
            }
        }

        public void Remove(Guid id)
        {
            using (var context = new PhotoManagerDbContext())
            {
                var entity = new PhotoEntity() { Id = id };
                context.Photos.Attach(entity);

                context.Photos.Remove(entity);
                context.SaveChanges();
            }
        }
        

        


    }
}
