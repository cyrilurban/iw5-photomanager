﻿using System;
using System.Collections.Generic;
using System.Linq;
using PhotoManager.BL.Mappers;
using PhotoManager.BL.Models;
using PhotoManager.DAL.Entities;
using PhotoManager.DAL;

namespace PhotoManager.BL.Repositories
{
    public class PersonTagRepository : IRepository<PersonTagDetailModel, PersonTagListModel>
    {
        private readonly PersonTagMapper personTagMapper = new PersonTagMapper();

        public List<PersonTagListModel> GetAll()
        {
            using (var context = new PhotoManagerDbContext())
            {
                return context.PersonTags.Select(personTagMapper.MapEntityToListModel).ToList();
            }
        }

        public PersonTagDetailModel GetById(Guid id)
        {
            using (var context = new PhotoManagerDbContext())
            {
                var item = context.PersonTags.FirstOrDefault(i => i.Id == id);

                if (item == null)
                {
                    return null;
                }

                return personTagMapper.MapEntityToDetailModel(item);
            }
        }

        public PersonTagDetailModel Insert(PersonTagDetailModel detail)
        {
            using (var context = new PhotoManagerDbContext())
            {
                var entity = personTagMapper.MapDetailModelToEntity(detail);
                entity.Id = Guid.NewGuid();

                var person = context.People.SingleOrDefault(i => i.Id == detail.Person.Id);
                var photo = context.Photos.SingleOrDefault(p => p.Id == detail.Photo.Id);
                var personTag = new PersonTagEntity
                {
                    Person = person,
                    Photo = photo,
                    PositionX = detail.PositionX,
                    PositionY = detail.PositionY
                };

                context.PersonTags.Add(personTag);

                context.SaveChanges();

                return personTagMapper.MapEntityToDetailModel(personTag);
            }
        }

        public void Remove(Guid id)
        {
            using (var context = new PhotoManagerDbContext())
            {
                var entity = new PersonTagEntity() { Id = id };
                context.PersonTags.Attach(entity);

                context.PersonTags.Remove(entity);
                context.SaveChanges();
            }
        }

        public PersonTagDetailModel Update(PersonTagDetailModel detail)
        {
            using (var context = new PhotoManagerDbContext())
            {
                var entity = context.PersonTags.SingleOrDefault(a => a.Id == detail.Id);

             //   entity.PersonId = detail.PersonId;
             //   entity.PhotoId = detail.PhotoId;
                entity.PositionX = detail.PositionX;
                entity.PositionY = detail.PositionY;

                context.SaveChanges();
                return personTagMapper.MapEntityToDetailModel(entity); 
            }
        }
    }
}
