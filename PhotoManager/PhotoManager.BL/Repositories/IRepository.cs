﻿using System;
using System.Collections.Generic;

namespace PhotoManager.BL.Repositories
{
    interface IRepository<DetailModel, ListModel>
    {
        List<ListModel> GetAll();
        DetailModel GetById(Guid id);
        DetailModel Insert(DetailModel detail);
      //  DetailModel Update(DetailModel detail);
        void Remove(Guid id);
    }
}
