﻿using PhotoManager.BL.Models.Base.Implementation;
using System;

namespace PhotoManager.BL.Models
{
    public class PhotoListModel: ModelBase
    {
        public string Name { get; set; }

        public string Format { get; set; }

        public int ResolutionX { get; set; }

        public int ResolutionY { get; set; }

        public DateTime Time { get; set; }
    }
}
