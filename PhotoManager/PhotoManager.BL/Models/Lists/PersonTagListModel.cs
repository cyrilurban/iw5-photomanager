﻿using PhotoManager.BL.Models.Base.Implementation;
using PhotoManager.DAL.Entities;

namespace PhotoManager.BL.Models
{
    public class PersonTagListModel: ModelBase
    {
        public string Firstname { get; set; }

        public string Surname { get; set; }

        public string Photoname { get; set; }

        public int PositionX { get; set; }

        public int PositionY { get; set; }
    }
}
