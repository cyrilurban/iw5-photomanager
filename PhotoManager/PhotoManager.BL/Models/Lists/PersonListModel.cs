﻿using PhotoManager.BL.Models.Base.Implementation;

namespace PhotoManager.BL.Models
{
    public class PersonListModel: ModelBase
    {
        public string Name { get; set; }
    }
}
