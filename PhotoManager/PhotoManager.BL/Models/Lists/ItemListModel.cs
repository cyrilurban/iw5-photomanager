﻿using PhotoManager.BL.Models.Base.Implementation;

namespace PhotoManager.BL.Models
{
    public class ItemListModel: ModelBase
    {
        public string Name { get; set; }
    }
}
