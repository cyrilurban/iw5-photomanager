﻿using PhotoManager.BL.Models.Base.Implementation;

namespace PhotoManager.BL.Models
{
    public class ItemTagListModel: ModelBase
    {
        public string Name { get; set; }

        public string Photoname { get; set; }
        public int PositionX { get; set; }

        public int PositionY { get; set; }
    }
}
