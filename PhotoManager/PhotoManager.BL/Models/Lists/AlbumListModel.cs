﻿using PhotoManager.BL.Models.Base.Implementation;

namespace PhotoManager.BL.Models
{
    public class AlbumListModel : ModelBase
    {
        public string Name { get; set; }

        public int NumberOfPhotos { get; set; }
    }
}
