﻿using PhotoManager.BL.Models.Base.Interface;
using System;

namespace PhotoManager.BL.Models.Base.Implementation
{
    public class ModelBase: IModelBase
    {
        public Guid Id { get; set; }
    }
}
