﻿using PhotoManager.BL.Models.Base.Implementation;
using PhotoManager.DAL.Entities;
using System.Collections.Generic;

namespace PhotoManager.BL.Models
{
    public class PersonDetailModel : ModelBase
    {
        public string Firstname { get; set; }

        public string Surname { get; set; }

        public virtual ICollection<PersonTagListModel> Photos { get; set; }
    }
}
