﻿using PhotoManager.BL.Models.Base.Implementation;
using PhotoManager.DAL.Entities;
using System;
using System.Collections.Generic;

namespace PhotoManager.BL.Models
{
    public class PhotoDetailModel: ModelBase
    {
        public string Name { get; set; }

        public byte[] Data { get; set; }
        public string ImagePath { get; set; }

        public string Format { get; set; }

        public int ResolutionX { get; set; }

        public int ResolutionY { get; set; }

        public DateTime Time { get; set; }

        public string Note { get; set; }

        public virtual ICollection<ItemTagListModel> ItemTags { get; set; }

        public virtual ICollection<PersonTagListModel> PersonTags { get; set; }

        public virtual ICollection<AlbumListModel> Albums { get; set; }
    }
}
