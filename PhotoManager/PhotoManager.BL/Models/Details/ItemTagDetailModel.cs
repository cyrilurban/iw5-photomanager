﻿using PhotoManager.BL.Models.Base.Implementation;
using PhotoManager.DAL.Entities;
using System;

namespace PhotoManager.BL.Models
{
    public class ItemTagDetailModel: ModelBase
    {
        public ItemDetailModel Item { get; set; }
        public Guid ItemId { get; set; }

        public int PositionX { get; set; }

        public int PositionY { get; set; }

        public virtual PhotoDetailModel Photo { get; set; }
        public Guid PhotoId { get; set; }
    }
}
