﻿using PhotoManager.BL.Models.Base.Implementation;
using PhotoManager.DAL.Entities;
using System;

namespace PhotoManager.BL.Models
{
    public class PersonTagDetailModel: ModelBase
    {
        public PersonDetailModel Person { get; set; }
        public Guid PersonId { get; set; }

        public int PositionX { get; set; }

        public int PositionY { get; set; }

        public PhotoDetailModel Photo { get; set; }
        public Guid PhotoId { get; set; }
    }
}
