﻿using PhotoManager.BL.Models.Base.Implementation;
using PhotoManager.DAL.Entities;
using System.Collections.Generic;

namespace PhotoManager.BL.Models
{
    public class ItemDetailModel: ModelBase
    {
        public string Name { get; set; }

        public virtual ICollection<ItemTagListModel> Photos { get; set; }
    }
}
