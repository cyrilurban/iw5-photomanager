﻿using PhotoManager.BL.Models.Base.Implementation;
using PhotoManager.DAL.Entities;
using System.Collections.Generic;

namespace PhotoManager.BL.Models
{
    public class AlbumDetailModel: ModelBase
    {
        public string Name { get; set; }

        public ICollection<PhotoListModel> Photos { get; set; }
    }
}
