﻿using PhotoManager.BL.Models;
using PhotoManager.BL.Repositories;
using PhotoManager.BL.Mappers;
using PhotoManager.DAL;
using PhotoManager.DAL.Entities;
using System;
using System.Linq;
using Xunit;

namespace PhotoManager.Tests
{
    public class PersonTagRepositoryTests
    {
        private readonly PersonRepository personRepository = new PersonRepository();
        private readonly PersonTagRepository personTagRepository = new PersonTagRepository();
        private readonly PhotoRepository photoRepository = new PhotoRepository();
        private readonly PersonTagMapper personTagMapper = new PersonTagMapper();
        private readonly PhotoMapper photoMapper = new PhotoMapper();
        private readonly PersonMapper personMapper = new PersonMapper();

        private PhotoEntity MakeTestPhoto()
        {
            return new PhotoEntity()
            {
                Format = "jpg",
                Name = "Sandy beach",
                Note = "Love on the beach",
                ResolutionX = 1920,
                ResolutionY = 1080,
                Time = DateTime.Now
            };
        }

        private PersonEntity MakeTestPerson()
        {
            return new PersonEntity()
            {
                Firstname = "Peter",
                Surname = "Smith"
            };
        }

        private PersonTagDetailModel CreateTestPersonTag(PersonDetailModel person, PhotoDetailModel photo)
        {
            PersonTagDetailModel personTag = new PersonTagDetailModel()
            {
                Person = person,
                PersonId = photo.Id,
                PositionX = 100,
                PositionY = 50,
                Photo = photo,
                PhotoId = photo.Id
            };

            return personTagRepository.Insert(personTag);

        }

        private void UpadateTagOnPhoto(PersonTagDetailModel personTag, PhotoEntity photo)
        {
            photo.PersonTags.Add(personTagMapper.MapDetailModelToEntity(personTag));
            var photoDetail = photoMapper.MapEntityToDetailModel(photo);
            photoRepository.Update(photoDetail);
        }

        [Fact]
        [AutoRollback]
        public void DbConnectionTest()
        {
            using (var photoManagerDbContext = new PhotoManagerDbContext())
            {
                photoManagerDbContext.PersonTags.Any();
            }
        }
    }
}
