﻿using PhotoManager.BL.Mappers;
using PhotoManager.BL.Models;
using PhotoManager.BL.Repositories;
using PhotoManager.DAL;
using PhotoManager.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace PhotoManager.Tests
{
    public class AlbumRepositoryTests
    {
        private readonly AlbumRepository albumRepository = new AlbumRepository();
        private readonly PhotoRepository photoRepository = new PhotoRepository();
        private readonly PhotoMapper mapper = new PhotoMapper();

        private AlbumDetailModel CreateTestAlbum(string albumName)
        {
            

            AlbumDetailModel album = new AlbumDetailModel()
            {
                Name = albumName
            };
           

            return albumRepository.Insert(album);
        }

        [Fact]
        [AutoRollback]
        public void DbConnectionTest()
        {
            using (var photoManagerDbContext = new PhotoManagerDbContext())
            {
                photoManagerDbContext.Albums.Any();
            }
        }
        
        [Fact]
        [AutoRollback]
        public void GetAll()
        {
            List<AlbumListModel> albumList = albumRepository.GetAll();
            Assert.NotNull(albumList);
        }

        [Fact]
        [AutoRollback]
        public void Insert_TestCount()
        {
            int albumListOldCount = albumRepository.GetAll().Count;

            AlbumDetailModel createdAlbum = CreateTestAlbum("Summer holiday");

            int countAlbumListAfterCreate = albumRepository.GetAll().Count();

            Assert.True(countAlbumListAfterCreate == albumListOldCount + 1, "The count of albums should be +1 after add.");           
        }
              
        [Fact]
        [AutoRollback]
        public void Insert_TestContainsInDB()
        {
            const string albumName = "Summer holiday";
            AlbumDetailModel createdAlbum = CreateTestAlbum(albumName);

            bool isAlbumInList = false;
            using (var photoManagerDbContext = new PhotoManagerDbContext())
            {
                isAlbumInList = photoManagerDbContext
                    .Albums
                    .Any(t => t.Name == albumName);
            }

            Assert.True(isAlbumInList, "The added album should be available to others in the database.");
        }

        [Fact]
        [AutoRollback]
        public void GetByName()
        {
            const string albumName = "Summer holiday";
            AlbumDetailModel createdAlbum = CreateTestAlbum(albumName);

            AlbumDetailModel foundAlbum = albumRepository.GetByName(albumName);

            Assert.NotNull(foundAlbum);
        }

        [Fact]
        [AutoRollback]
        public void GetByName_CompareName()
        {
            const string albumName = "Summer holiday";
            AlbumDetailModel createdAlbum = CreateTestAlbum(albumName);

            AlbumDetailModel foundAlbum = albumRepository.GetByName(albumName);

            Assert.True(albumName == foundAlbum.Name, "The added album should be available for find.");
        }

        [Fact]
        [AutoRollback]
        public void GetByName_EmptyStringArgument()
        {
            string albumName = "";
            Assert.Throws<ArgumentException>(() => albumRepository.GetByName(albumName));
        }

        [Fact]
        [AutoRollback]
        public void GetByName_WhiteSpaceStringArgument()
        {
            string albumName = " ";
            Assert.Throws<ArgumentException>(() => albumRepository.GetByName(albumName));
        }

        [Fact]
        [AutoRollback]
        public void GetByName_NullStringArgument()
        {
            string albumName = null;
            Assert.Throws<ArgumentNullException>(() => albumRepository.GetByName(albumName));
        }

        [Fact]
        [AutoRollback]
        public void GetById()
        {
            AlbumDetailModel createdAlbum = CreateTestAlbum("Summer holiday");

            AlbumDetailModel foundAlbumById = albumRepository.GetById(createdAlbum.Id);

            Assert.NotNull(foundAlbumById);
        }

        [Fact]
        [AutoRollback]
        public void GetById_CompareId()
        {
            AlbumDetailModel createdAlbum = CreateTestAlbum("Summer holiday");

            AlbumDetailModel foundAlbumById = albumRepository.GetById(createdAlbum.Id);

            Assert.True(createdAlbum.Id == foundAlbumById.Id, "The Ids of album should be equal.");
        }

        [Fact]
        [AutoRollback]
        public void GetById_CompareName()
        {
            AlbumDetailModel createdAlbum = CreateTestAlbum("Summer holiday");

            AlbumDetailModel foundAlbumById = albumRepository.GetById(createdAlbum.Id);

            Assert.True(createdAlbum.Name == foundAlbumById.Name, "The names of album should be equal.");
        }

        [Fact]
        [AutoRollback]
        public void GetById_NonExistAlbumGuid()
        {
            // new not existing Guid of album
            Guid albumId = Guid.NewGuid();

            Assert.Null(albumRepository.GetById(albumId));
        }

        [Fact]
        [AutoRollback]
        public void Remove()
        {
            AlbumDetailModel createdAlbum = CreateTestAlbum("Summer holiday");
            int albumListOldCount = albumRepository.GetAll().Count;

            albumRepository.Remove(createdAlbum.Id);
            int countAlbumListAfterRemove = albumRepository.GetAll().Count();

            Assert.True(countAlbumListAfterRemove == albumListOldCount - 1, "The count of albums should be -1 after remove.");
        }

        [Fact]
        [AutoRollback]
        public void Update()
        {
            AlbumDetailModel createdAlbum = CreateTestAlbum("Summer holiday");

            const string updatedName = "Winter holiday";
            createdAlbum.Name = updatedName;
            albumRepository.Update(createdAlbum);

            AlbumDetailModel foundAlbumById = albumRepository.GetById(createdAlbum.Id);

            Assert.True(updatedName == foundAlbumById.Name, "The names of album should be equal  after update.");
        }
    }
}
