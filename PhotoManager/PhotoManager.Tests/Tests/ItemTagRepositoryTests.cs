﻿using PhotoManager.BL.Models;
using PhotoManager.BL.Repositories;
using PhotoManager.BL.Mappers;
using PhotoManager.DAL;
using PhotoManager.DAL.Entities;
using System;
using System.Linq;
using Xunit;

namespace PhotoManager.Tests
{
    public class ItemTagRepositoryTests
    {
        private readonly ItemRepository itemRepository = new ItemRepository();
        private readonly ItemTagRepository itemTagRepository = new ItemTagRepository();
        private readonly PhotoRepository photoRepository = new PhotoRepository();
        private readonly ItemTagMapper itemTagMapper = new ItemTagMapper();
        private readonly PhotoMapper photoTagMapper = new PhotoMapper();
        private readonly ItemMapper itemMapper = new ItemMapper();
        
        private PhotoEntity MakeTestPhoto()
        {
            return new PhotoEntity()
            {
                Format = "jpg",
                Name = "Sandy beach",
                Note = "Love on the beach",
                ResolutionX = 1920,
                ResolutionY = 1080,
                Time = DateTime.Now
            };
        }

        private ItemEntity MakeTestItem()
        {
            return new ItemEntity()
            {
                Name = "Table"
            };
        }

        private ItemTagDetailModel CreateTestItemTag(ItemDetailModel item, PhotoDetailModel photo)
        {
            ItemTagDetailModel itemTag = new ItemTagDetailModel()
            {
                Item = item,
                ItemId = photo.Id,
                PositionX = 100,
                PositionY = 50,
                Photo = photo,
                PhotoId = photo.Id
            };

            return itemTagRepository.Insert(itemTag);
        }

        private void UpadateTagOnPhoto(ItemTagDetailModel itemTag, PhotoEntity photo)
        {
            photo.ItemTags.Add(itemTagMapper.MapDetailModelToEntity(itemTag));
            var photoDetail = photoTagMapper.MapEntityToDetailModel(photo);
            photoRepository.Update(photoDetail);
        }

        [Fact]
        [AutoRollback]
        public void DbConnectionTest()
        {
            using (var photoManagerDbContext = new PhotoManagerDbContext())
            {
                photoManagerDbContext.ItemTags.Any();
            }
        }
    }
}
