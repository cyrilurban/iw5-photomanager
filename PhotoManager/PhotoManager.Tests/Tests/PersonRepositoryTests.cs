﻿using PhotoManager.BL.Models;
using PhotoManager.BL.Repositories;
using PhotoManager.DAL;
using System;
using System.Linq;
using Xunit;

namespace PhotoManager.Tests
{
    public class PersonRepositoryTests
    {
        private readonly PersonRepository personRepository = new PersonRepository();

        private PersonDetailModel CreateTestPerson()
        {
            PersonDetailModel person = new PersonDetailModel()
            {
                Firstname = "Peter",
                Surname = "Smith"
            };

            return personRepository.Insert(person);
        }

        [Fact]
        [AutoRollback]
        public void DbConnectionTest()
        {
            using (var photoManagerDbContext = new PhotoManagerDbContext())
            {
                photoManagerDbContext.People.Any();
            }
        }

        [Fact]
        [AutoRollback]
        public void GetAll()
        {
            var peopleList = personRepository.GetAll();
            Assert.NotNull(peopleList);
        }

        [Fact]
        [AutoRollback]
        public void Insert_TestCount()
        {
            int personListOldCount = personRepository.GetAll().Count;

            PersonDetailModel createdPerson = CreateTestPerson();

            int countPersonListAfterCreate = personRepository.GetAll().Count();

            Assert.True(countPersonListAfterCreate == personListOldCount + 1, "The count of people should be +1 after add.");
        }

        [Fact]
        [AutoRollback]
        public void Insert_TestContainsInDB()
        {
            PersonDetailModel createdPerson = CreateTestPerson();

            bool isPersonInList = false;
            using (var photoManagerDbContext = new PhotoManagerDbContext())
            {
                isPersonInList = photoManagerDbContext
                    .People
                    .Any(t => t.Id == createdPerson.Id);
            }

            Assert.True(isPersonInList, "The added person should be available to others in the database.");
        }

        [Fact]
        [AutoRollback]
        public void GetById()
        {
            PersonDetailModel createdPerson = CreateTestPerson();

            PersonDetailModel foundPersonById = personRepository.GetById(createdPerson.Id);

            Assert.NotNull(foundPersonById);
        }

        [Fact]
        [AutoRollback]
        public void GetById_CompareId()
        {
            PersonDetailModel createdPerson = CreateTestPerson();

            PersonDetailModel foundPersonById = personRepository.GetById(createdPerson.Id);

            Assert.True(createdPerson.Id == foundPersonById.Id, "The Ids of people should be equal.");
        }

        [Fact]
        [AutoRollback]
        public void GetById_CompareFirstname()
        {
            PersonDetailModel createdPerson = CreateTestPerson();

            PersonDetailModel foundPersonById = personRepository.GetById(createdPerson.Id);

            Assert.True(createdPerson.Firstname == foundPersonById.Firstname, "The names of people should be equal.");
        }

        [Fact]
        [AutoRollback]
        public void GetById_NonExistPersonGuid()
        {
            // new not existing Guid of person
            Guid personId = Guid.NewGuid();

            Assert.Null(personRepository.GetById(personId));
        }

        [Fact]
        [AutoRollback]
        public void Remove()
        {
            PersonDetailModel createdPerson = CreateTestPerson();
            int personListOldCount = personRepository.GetAll().Count;

            personRepository.Remove(createdPerson.Id);
            int countPersonListAfterRemove = personRepository.GetAll().Count();

            Assert.True(countPersonListAfterRemove == personListOldCount - 1, "The count of people should be -1 after remove.");
        }

        [Fact]
        [AutoRollback]
        public void Update()
        {
            PersonDetailModel createdPerson = CreateTestPerson();

            const string updatedFirstname = "John";
            createdPerson.Firstname = updatedFirstname;
            personRepository.Update(createdPerson);

            PersonDetailModel foundAlbumById = personRepository.GetById(createdPerson.Id);

            Assert.True(updatedFirstname == foundAlbumById.Firstname, "The names of people should be equal after update.");
        }
    }
}
