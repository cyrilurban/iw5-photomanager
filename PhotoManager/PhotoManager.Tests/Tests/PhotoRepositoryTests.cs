﻿using PhotoManager.BL.Mappers;
using PhotoManager.BL.Models;
using PhotoManager.BL.Repositories;
using PhotoManager.DAL;
using PhotoManager.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace PhotoManager.Tests
{
    public class PhotoRepositoryTests
    {
        private readonly PhotoRepository photoRepository = new PhotoRepository();
        private readonly PhotoMapper photoMapper = new PhotoMapper();

        private PhotoDetailModel CreateTestPhoto(string photoName)
        {
            PhotoDetailModel photo = new PhotoDetailModel()
            {
                Name = photoName
            };

            return photoRepository.Insert(photo);
        }

        private PhotoDetailModel CreateTestPhoto()
        {
            var photo = new PhotoEntity()
            {
                Format = "jpg",
                Name = "Sandy beach",
                Note = "Love on the beach",
                ResolutionX = 1920,
                ResolutionY = 1080,
                Time = DateTime.Now
            };

            return photoRepository.Insert(photoMapper.MapEntityToDetailModel(photo));
        }

        [Fact]
        [AutoRollback]
        public void DbConnectionTest()
        {
            using (var photoManagerDbContext = new PhotoManagerDbContext())
            {
                photoManagerDbContext.Photos.Any();
            }
        }

        [Fact]
        [AutoRollback]
        public void GetAll()
        {
            List<PhotoListModel> photoList = photoRepository.GetAll();
            Assert.NotNull(photoList);
        }

        [Fact]
        [AutoRollback]
        public void Insert_TestContainsInDB()
        {
            PhotoDetailModel createdPhoto = CreateTestPhoto();

            bool isPhotoInList = false;
            using (var photoManagerDbContext = new PhotoManagerDbContext())
            {
                isPhotoInList = photoManagerDbContext
                    .Photos
                    .Any(t => t.Name == createdPhoto.Name);
            }

            Assert.True(isPhotoInList, "The added photo should be available to others in the database.");
        }

        [Fact]
        [AutoRollback]
        public void GetById()
        {
            PhotoDetailModel createdPhoto = CreateTestPhoto();

            PhotoDetailModel foundPhotoById = photoRepository.GetById(createdPhoto.Id);

            Assert.NotNull(foundPhotoById);
        }

        [Fact]
        [AutoRollback]
        public void GetById_CompareId()
        {
            PhotoDetailModel createdPhoto = CreateTestPhoto();

            PhotoDetailModel foundPhotoById = photoRepository.GetById(createdPhoto.Id);

            Assert.True(createdPhoto.Id == foundPhotoById.Id, "The Ids of photo should be equal.");
        }

        [Fact]
        [AutoRollback]
        public void GetById_CompareName()
        {
            PhotoDetailModel createdPhoto = CreateTestPhoto();

            PhotoDetailModel foundPhotoById = photoRepository.GetById(createdPhoto.Id);

            Assert.True(createdPhoto.Name == foundPhotoById.Name, "The names of photo should be equal.");
        }

        [Fact]
        [AutoRollback]
        public void GetById_NonExistPhotoGuid()
        {
            // new not existing Guid of photo
            Guid photoId = Guid.NewGuid();

            Assert.Null(photoRepository.GetById(photoId));
        }

        [Fact]
        [AutoRollback]
        public void Remove()
        {
            PhotoDetailModel createdPhoto = CreateTestPhoto();
            int photoListOldCount = photoRepository.GetAll().Count;

            photoRepository.Remove(createdPhoto.Id);
            int countPhotoListAfterRemove = photoRepository.GetAll().Count();

            Assert.True(countPhotoListAfterRemove == photoListOldCount - 1, "The count of photos should be -1 after remove.");
        }

        [Fact]
        [AutoRollback]
        public void Update()
        {
            PhotoDetailModel createdPhoto = CreateTestPhoto();

            const string updatedName = "Winter holiday";
            createdPhoto.Name = updatedName;
            photoRepository.Update(createdPhoto);

            PhotoDetailModel foundPhotoById = photoRepository.GetById(createdPhoto.Id);

            Assert.True(updatedName == foundPhotoById.Name, "The names of photo should be equal  after update.");
        }
    }
}
