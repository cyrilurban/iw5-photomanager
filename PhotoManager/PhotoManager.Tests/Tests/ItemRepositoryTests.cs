﻿using PhotoManager.BL.Models;
using PhotoManager.DAL;
using PhotoManager.BL.Repositories;
using System;
using System.Linq;
using Xunit;

namespace PhotoManager.Tests
{
    public class ItemRepositoryTests
    {
        private readonly ItemRepository itemRepository = new ItemRepository();

        private ItemDetailModel CreateTestItem()
        {
            ItemDetailModel item = new ItemDetailModel()
            {
                Name = "Table"
            };

            return itemRepository.Insert(item);
        }

        [Fact]
        [AutoRollback]
        public void DbConnectionTest()
        {
            using (var photoManagerDbContext = new PhotoManagerDbContext())
            {
                photoManagerDbContext.Items.Any();
            }
        }

        [Fact]
        [AutoRollback]
        public void GetAll()
        {
            var itemsList = itemRepository.GetAll();
            Assert.NotNull(itemsList);
        }

        [Fact]
        [AutoRollback]
        public void Insert_TestCount()
        {
            int itemListOldCount = itemRepository.GetAll().Count;

            ItemDetailModel createdItem = CreateTestItem();

            int countItemListAfterCreate = itemRepository.GetAll().Count();

            Assert.True(countItemListAfterCreate == itemListOldCount + 1, "The count of items should be +1 after add.");
        }

        [Fact]
        [AutoRollback]
        public void Insert_TestContainsInDB()
        {
            ItemDetailModel createdItem = CreateTestItem();

            bool isItemInList = false;
            using (var photoManagerDbContext = new PhotoManagerDbContext())
            {
                isItemInList = photoManagerDbContext
                    .Items
                    .Any(t => t.Id == createdItem.Id);
            }

            Assert.True(isItemInList, "The added item should be available to others in the database.");
        }

        [Fact]
        [AutoRollback]
        public void GetById()
        {
            ItemDetailModel createdItem = CreateTestItem();

            ItemDetailModel foundItemById = itemRepository.GetById(createdItem.Id);

            Assert.NotNull(foundItemById);
        }

        [Fact]
        [AutoRollback]
        public void GetById_CompareId()
        {
            ItemDetailModel createdItem = CreateTestItem();

            ItemDetailModel foundItemById = itemRepository.GetById(createdItem.Id);

            Assert.True(createdItem.Id == foundItemById.Id, "The Ids of items should be equal.");
        }

        [Fact]
        [AutoRollback]
        public void GetById_CompareName()
        {
            ItemDetailModel createdItem = CreateTestItem();

            ItemDetailModel foundItemById = itemRepository.GetById(createdItem.Id);

            Assert.True(createdItem.Name == foundItemById.Name, "The names of items should be equal.");
        }

        [Fact]
        [AutoRollback]
        public void GetById_NonExistItemGuid()
        {
            // new not existing Guid of item
            Guid itemId = Guid.NewGuid();

            Assert.Null(itemRepository.GetById(itemId));
        }

        [Fact]
        [AutoRollback]
        public void Remove()
        {
            ItemDetailModel createdItem = CreateTestItem();
            int itemListOldCount = itemRepository.GetAll().Count;

            itemRepository.Remove(createdItem.Id);
            int countItemListAfterRemove = itemRepository.GetAll().Count();

            Assert.True(countItemListAfterRemove == itemListOldCount - 1, "The count of items should be -1 after remove.");
        }

        [Fact]
        [AutoRollback]
        public void Update()
        {
            ItemDetailModel createdItem = CreateTestItem();

            const string updatedName = "Chair";
            createdItem.Name = updatedName;
            itemRepository.Update(createdItem);

            ItemDetailModel foundAlbumById = itemRepository.GetById(createdItem.Id);

            Assert.True(updatedName == foundAlbumById.Name, "The names of items should be equal after update.");
        }
    }
}
