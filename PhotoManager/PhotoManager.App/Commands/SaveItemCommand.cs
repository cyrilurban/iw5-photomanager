﻿using System;
using System.Windows.Input;
using PhotoManager.App.ViewModels;
using PhotoManager.BL;
using PhotoManager.BL.Messages;
using PhotoManager.BL.Models;
using PhotoManager.BL.Repositories;

namespace PhotoManager.App.Commands
{
    public class SaveItemCommand : ICommand
    {
        private readonly ItemRepository itemRepository;
        private readonly ItemDetailViewModel viewModel;
        private readonly IMessenger messenger;

        public SaveItemCommand(ItemRepository itemRepository, ItemDetailViewModel viewModel, IMessenger messenger)
        {
            this.itemRepository = itemRepository;
            this.viewModel = viewModel;
            this.messenger = messenger;
        }

        public bool CanExecute(object parameter)
        {
            return viewModel.Detail?.Name != null;
        }

        public void Execute(object parameter)
        {
            if (viewModel.Detail.Id == Guid.Empty)
            {
                viewModel.Detail = itemRepository.Insert(viewModel.Detail);
            }
            else
            {
                itemRepository.Update(viewModel.Detail);
            }

            messenger.Send(new UpdatedItemMessage(viewModel.Detail));
        }

        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }
    }
}