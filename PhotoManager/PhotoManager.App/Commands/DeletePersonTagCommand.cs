﻿using PhotoManager.App.ViewModels;
using PhotoManager.BL;
using PhotoManager.BL.Messages;
using PhotoManager.BL.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace PhotoManager.App.Commands
{
    public class DeletePersonTagCommand : ICommand
    {
        private PhotoDetailViewModel photoView;
        private readonly IMessenger messenger;
        private readonly PersonTagRepository personTagRepository;
        private readonly PhotoRepository photoRepository;

        public DeletePersonTagCommand(PhotoDetailViewModel photoView, IMessenger messenger, PersonTagRepository personTagRepository, PhotoRepository photoRepository)
        {
            this.photoView = photoView;
            this.messenger = messenger;
            this.personTagRepository = personTagRepository;
            this.photoRepository = photoRepository;
        }

        public bool CanExecute(object parameter)
        {
            //var can = true;
            return photoView.SelectedPersonTag != null;
            //return can;
        }

        public void Execute(object parameter)
        {
            personTagRepository.Remove(photoView.SelectedPersonTag.Id);
            messenger.Send(new DeletedPersonTagMessage(photoView.SelectedPersonTag.Id));
            photoView.Detail = photoRepository.GetById(photoView.Detail.Id);
        }

        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }
    }
}
