﻿using PhotoManager.App.ViewModels;
using PhotoManager.BL;
using PhotoManager.BL.Messages;
using PhotoManager.BL.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace PhotoManager.App.Commands
{
    public class AddItemToPhotoCommand : ICommand
    {
        private PhotoDetailViewModel viewModel;
        private PhotoRepository photoRepository;
        private readonly IMessenger messenger;
        public AddItemToPhotoCommand(PhotoDetailViewModel viewModel, PhotoRepository photoRepository, IMessenger messenger )
        {
            this.viewModel = viewModel;
            this.photoRepository = photoRepository;
            this.messenger = messenger;
        }

        public bool CanExecute(object parameter)
        {
            if(viewModel.Detail != null)
            {
                var photo = photoRepository.GetById(viewModel.Detail.Id);
                return photo == null ? false : true;
            }
            return false;
            
        }

        public void Execute(object parameter)
        {
            viewModel.ShowDetail = false;
            messenger.Send(new NewItemInPhotoMessage(viewModel.Detail));
        }

        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }
    }
}
