﻿using PhotoManager.App.ViewModels;
using PhotoManager.BL;
using PhotoManager.BL.Messages;
using PhotoManager.BL.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace PhotoManager.App.Commands
{
    public class DeleteItemTagCommand : ICommand
    {
        private PhotoDetailViewModel photoView;
        private readonly IMessenger messenger;
        private readonly ItemTagRepository itemTagRepository;
        private readonly PhotoRepository photoRepository;

        public DeleteItemTagCommand(PhotoDetailViewModel photoView, IMessenger messenger, ItemTagRepository itemTagRepository, PhotoRepository photoRepository)
        {
            this.photoView = photoView;
            this.messenger = messenger;
            this.itemTagRepository = itemTagRepository;
            this.photoRepository = photoRepository;
        }

        public bool CanExecute(object parameter)
        {
            var can = true;
            can &= photoView.SelectedItemTag != null;
            return can;
        }

        public void Execute(object parameter)
        {
            itemTagRepository.Remove(photoView.SelectedItemTag.Id);
            messenger.Send(new DeletedItemTagMessage(photoView.SelectedItemTag.Id));
            photoView.Detail = photoRepository.GetById(photoView.Detail.Id);
            photoView.SelectedItemTag = null;
        }

        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }
    }
}
