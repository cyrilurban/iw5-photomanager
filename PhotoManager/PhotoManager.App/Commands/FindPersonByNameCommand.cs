﻿using PhotoManager.App.ViewModels;
using PhotoManager.BL.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace PhotoManager.App.Commands
{
    public class FindPersonByNameCommand : ICommand
    {
        private PersonListViewModel viewModel;
        private readonly PersonRepository personRepository;

        public FindPersonByNameCommand(PersonListViewModel viewModel, PersonRepository personRepository)
        {
            this.viewModel = viewModel;
            this.personRepository = personRepository;
        }

        public bool CanExecute(object parameter)
        {
            return viewModel.Firstname != "" && viewModel.Surname != "";
        }

        public void Execute(object parameter)
        {

            var people = personRepository.GetByBothNames(viewModel.Firstname, viewModel.Surname);
            viewModel.Persons.Clear();
            foreach (var p in people)
            {
                viewModel.Persons.Add(p);
            }
        }

        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }
    
    }
}
