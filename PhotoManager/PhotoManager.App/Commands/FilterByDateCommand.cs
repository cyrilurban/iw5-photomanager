﻿using PhotoManager.App.ViewModels;
using PhotoManager.BL.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace PhotoManager.App.Commands
{
    public class FilterByDateCommand : ICommand
    {
        private PhotoListViewModel viewModel;
        private readonly PhotoRepository photoRepository;

        public FilterByDateCommand(PhotoListViewModel viewModel, PhotoRepository photoRepository)
        {
            this.viewModel = viewModel;
            this.photoRepository = photoRepository;
        }

        public bool CanExecute(object parameter)
        {
            return viewModel.Date != null;
        }

        public void Execute(object parameter)
        {
            var photos = photoRepository.FilterByTime((DateTime)viewModel.Date);
            viewModel.Photos.Clear();
            foreach (var p in photos)
            {
                viewModel.Photos.Add(p);
            }
        }
        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

    }
}
