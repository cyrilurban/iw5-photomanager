﻿using System;
using System.Windows.Input;
using PhotoManager.App.ViewModels;
using PhotoManager.BL;
using PhotoManager.BL.Messages;
using PhotoManager.BL.Models;
using PhotoManager.BL.Repositories;

namespace PhotoManager.App.Commands
{
    public class SavePersonCommand : ICommand
    {
        private readonly PersonRepository personRepository;
        private readonly PersonDetailViewModel viewModel;
        private readonly IMessenger messenger;

        public SavePersonCommand(PersonRepository personRepository, PersonDetailViewModel viewModel, IMessenger messenger)
        {
            this.personRepository = personRepository;
            this.viewModel = viewModel;
            this.messenger = messenger;
        }

        public bool CanExecute(object parameter)
        {
            bool can = true;

            can &= viewModel.Detail?.Firstname != null;
            can &= viewModel.Detail?.Surname != null;

            return can;
        }

        public void Execute(object parameter)
        {
            if (viewModel.Detail.Id == Guid.Empty)
            {
                viewModel.Detail = personRepository.Insert(viewModel.Detail);
            }
            else
            {
                personRepository.Update(viewModel.Detail);
            }

            messenger.Send(new UpdatedPersonMessage(viewModel.Detail));
        }

        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }
    }
}