﻿using PhotoManager.App.ViewModels;
using PhotoManager.BL.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace PhotoManager.App.Commands
{
    public class FindItemByNameCommand : ICommand
    {
        private ItemListViewModel viewModel;
        private readonly ItemRepository itemRepository;
        
        public FindItemByNameCommand(ItemListViewModel viewModel, ItemRepository itemRepository)
        {
            this.viewModel = viewModel;
            this.itemRepository = itemRepository;
        }

        public bool CanExecute(object parameter)
        {
            return viewModel.Name != "";
        }

        public void Execute(object parameter)
        {
            var items = itemRepository.GetByName(viewModel.Name);
            viewModel.Items.Clear();
            foreach(var i in items)
            {
                viewModel.Items.Add(i);
            }
        }

        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }
    }
}
