﻿using PhotoManager.App.ViewModels;
using PhotoManager.BL.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace PhotoManager.App.Commands
{
    public class FindPhotoByNameCommand : ICommand
    {
        private PhotoListViewModel viewModel;
        private readonly PhotoRepository photoRepository;
        public FindPhotoByNameCommand(PhotoListViewModel viewModel, PhotoRepository photoRepository)
        {
            this.viewModel = viewModel;
            this.photoRepository = photoRepository;
        }

        public bool CanExecute(object parameter)
        {
            return viewModel.Name != "";
        }

        public void Execute(object parameter)
        {
            var photos = photoRepository.GetByName(viewModel.Name);
            viewModel.Photos.Clear();
            foreach (var p in photos)
            {
                viewModel.Photos.Add(p);
            }
        }

        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }
    }
}
