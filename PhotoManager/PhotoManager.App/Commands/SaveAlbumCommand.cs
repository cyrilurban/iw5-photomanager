﻿using System;
using System.Windows.Input;
using PhotoManager.App.ViewModels;
using PhotoManager.BL;
using PhotoManager.BL.Messages;
using PhotoManager.BL.Models;
using PhotoManager.BL.Repositories;

namespace PhotoManager.App.Commands
{
    public class SaveAlbumCommand : ICommand
    {
        private readonly AlbumRepository albumRepository;
        private readonly AlbumDetailViewModel viewModel;
        private readonly IMessenger messenger;

        public SaveAlbumCommand(AlbumRepository albumRepository, AlbumDetailViewModel viewModel, IMessenger messenger)
        {
            this.albumRepository = albumRepository;
            this.viewModel = viewModel;
            this.messenger = messenger;
        }

        public bool CanExecute(object parameter)
        {
            return viewModel.Detail?.Name != null;
        }

        public void Execute(object parameter)
        {
            if (viewModel.Detail.Id == Guid.Empty)
            {
               viewModel.Detail = albumRepository.Insert(viewModel.Detail);
            }
            else
            {
                 albumRepository.Update(viewModel.Detail);
            }

            messenger.Send(new UpdatedAlbumMessage(viewModel.Detail));
        }

        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }
    }
}