﻿using PhotoManager.App.ViewModels;
using PhotoManager.BL;
using PhotoManager.BL.Messages;
using PhotoManager.BL.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace PhotoManager.App.Commands
{
    public class AddPhotoToAlbumCommand : ICommand
    {
        private AlbumDetailViewModel viewModel;
        private readonly AlbumRepository albumRepository;
        private readonly IMessenger messenger;

        public AddPhotoToAlbumCommand(AlbumDetailViewModel viewModel, AlbumRepository albumRepository, IMessenger messenger)
        {
            this.viewModel = viewModel;
            this.messenger = messenger;
            this.albumRepository = albumRepository;
        }

        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

        public bool CanExecute(object parameter)
        {
            if (viewModel.Detail != null)
            {
                var album = albumRepository.GetById(viewModel.Detail.Id);
                return album == null ? false : true;
            }
            return false;
        }

        public void Execute(object parameter)
        {
            viewModel.ShowDetail = false;
            messenger.Send(new NewPhotoInAlbumMessage());
        }
    }
}
