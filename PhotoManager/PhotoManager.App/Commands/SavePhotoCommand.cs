﻿using System;
using System.Windows.Input;
using PhotoManager.App.ViewModels;
using PhotoManager.BL;
using PhotoManager.BL.Messages;
using PhotoManager.BL.Models;
using PhotoManager.BL.Repositories;

namespace PhotoManager.App.Commands
{
    public class SavePhotoCommand : ICommand
    {
        private readonly PhotoRepository photoRepository;
        private readonly PhotoDetailViewModel viewModel;
        private readonly IMessenger messenger;

        public SavePhotoCommand(PhotoRepository photoRepository, PhotoDetailViewModel viewModel, IMessenger messenger)
        {
            this.photoRepository = photoRepository;
            this.viewModel = viewModel;
            this.messenger = messenger;
        }

        public bool CanExecute(object parameter)
        {
            bool can = true;

         //   can &= viewModel.Detail?.Data != null;
         //   can &= viewModel.Detail?.Albums != null;
            can &= viewModel.Detail?.Name != null;
            can &= viewModel.Detail?.Time != null;
            can &= viewModel.Detail?.Format != null;
            can &= viewModel.Detail?.ResolutionX != null;
            can &= viewModel.Detail?.ResolutionY != null;

            return can;
        }

        public void Execute(object parameter)
        {
            if (viewModel.Detail.Id == Guid.Empty)
            {
                viewModel.Detail = photoRepository.Insert(viewModel.Detail);
            }
            else
            {
                viewModel.Detail = photoRepository.Update(viewModel.Detail);
            }

            messenger.Send(new UpdatedPhotoMessage(viewModel.Detail));
        }

        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }
    }
}