﻿using System;
using System.Windows.Input;
using PhotoManager.DAL.Entities;

namespace PhotoManager.App.Commands
{
    class RelayCommand : ICommand
    {
        private readonly Action<object> executeAction;
        private readonly Func<object, bool> canExecuteAction;
        private PersonTagEntity selectedPersonTag;
        private string orderByName;

        public RelayCommand(Action<object> executeAction, Func<object, bool> canExecuteAction = null)
        {
            this.executeAction = executeAction;
            this.canExecuteAction = canExecuteAction;
        }

        public RelayCommand(Action executeAction, Func<bool> canExecuteAction = null)
            : this(p => executeAction(), p => canExecuteAction?.Invoke() ?? true)
        {

        }

        public RelayCommand(PersonTagEntity selectedPersonTag)
        {
            this.selectedPersonTag = selectedPersonTag;
        }

        public RelayCommand(string orderByName)
        {
            this.orderByName = orderByName;
        }

        public bool CanExecute(object parameter)
        {
            return canExecuteAction?.Invoke(parameter) ?? true;
        }

        public void Execute(object parameter)
        {
            executeAction?.Invoke(parameter);
        }

        public void RaiseCanExecuteChanged()
        {
            CanExecuteChanged?.Invoke(this, EventArgs.Empty);
        }

        public event EventHandler CanExecuteChanged;
    }
}
