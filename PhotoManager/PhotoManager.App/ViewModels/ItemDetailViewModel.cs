﻿using System;
using System.Collections.ObjectModel;
using System.Windows.Input;
using PhotoManager.App.Commands;
using PhotoManager.BL;
using PhotoManager.BL.Messages;
using PhotoManager.BL.Models;
using PhotoManager.BL.Repositories;
using PhotoManager.DAL.Entities;
using PhotoManager.BL.Mappers;

namespace PhotoManager.App.ViewModels
{
    public class ItemDetailViewModel : ViewModelBase
    {
        private readonly ItemRepository itemRepository;
        private readonly IMessenger messenger;
        private ItemDetailModel detail;
        private readonly ItemTagMapper itemTagMapper;

        public ICommand SelectPhotoCommand { get; }

        public ItemDetailModel Detail
        {
            get { return detail; }
            set
            {
                if (Equals(value, Detail)) return;

                detail = value;
                OnPropertyChanged();
            }
        }


        public ICommand SaveItemCommand { get; set; }
        public ICommand DeleteItemCommand { get; set; }
        public ICommand AddItemToPhotoCommand { get; set; }

        public ItemDetailViewModel(ItemRepository itemRepository, IMessenger messenger, ItemTagMapper itemTagMapper)
        {
            this.itemRepository = itemRepository;
            this.itemTagMapper = itemTagMapper;
            this.messenger = messenger;
           
            SaveItemCommand = new SaveItemCommand(itemRepository, this, messenger);
            DeleteItemCommand = new RelayCommand(DeleteItem);
            SelectPhotoCommand = new RelayCommand(PhotoSelectionChanged);
            

            this.messenger.Register<SelectedItemMessage>(SelectedItem);
            this.messenger.Register<NewItemMessage>(NewItemMessageReceived);
            this.messenger.Register<UpdatedItemTagMessage>(UpdatedItemTagMessageReceived);
            this.messenger.Register<DeletedItemTagMessage>(DeletedItemTagMessageReceived);
        }

        private void DeletedItemTagMessageReceived(DeletedItemTagMessage message)
        {
            if(Detail != null)
            {
                Detail = itemRepository.GetById(Detail.Id);
            }
            
        }

        private void UpdatedItemTagMessageReceived(UpdatedItemTagMessage message)
        {
            Detail = itemRepository.GetById(message.itemTag.Item.Id);
        }

        public void OnLoad()
        {
        }

        public void PhotoSelectionChanged(object listItem)
        {
            if (listItem is PhotoListModel photo)
            {
                messenger.Send(new SelectedPhotoMessage { Id = photo.Id });
            }

        }

        private void DeleteItem()
        {
            if (Detail.Id != Guid.Empty)
            {
                var itemId = Detail.Id;

                Detail = new ItemDetailModel();
                itemRepository.Remove(itemId);
                messenger.Send(new DeletedItemMessage(itemId));
            }
        }

        private void NewItemMessageReceived(NewItemMessage message)
        {
            Detail = new ItemDetailModel();
        }

        private void SelectedItem(SelectedItemMessage message)
        {
            Detail = itemRepository.GetById(message.Id);
            
        }
    }
}