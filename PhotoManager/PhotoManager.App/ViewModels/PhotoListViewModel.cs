﻿using PhotoManager.App.Commands;
using PhotoManager.BL;
using PhotoManager.BL.Mappers;
using PhotoManager.BL.Messages;
using PhotoManager.BL.Models;
using PhotoManager.BL.Repositories;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace PhotoManager.App.ViewModels
{
    public class PhotoListViewModel : ViewModelBase
    {
        private readonly PhotoRepository photoRepository;
        private readonly IMessenger messenger;
        private readonly PhotoMapper mapper;
        private bool showView;
        private string format;
        private int resolutionX;
        private int resolutionY;
        private DateTime? date;
        private string name;
        public bool ShowView
        {
            get { return showView; }
            set
            {
                showView = value;
                OnPropertyChanged();
            }
        }

        public string Format
        {
            get { return format; }
            set
            {
                format = value;
                OnPropertyChanged();
            }
        }

        public int ResolutionX
        {
            get { return resolutionX; }
            set
            {
                resolutionX = value;
                OnPropertyChanged();
            }
        }

        public int ResolutionY
        {
            get { return resolutionY; }
            set
            {
                resolutionY = value;
                OnPropertyChanged();
            }
        }
        public string Name
        {
            get { return name; }
            set
            {
                name = value;
                OnPropertyChanged();
            }
        }

        public DateTime? Date
        {
            get { return date; }
            set
            {
                date = value;
                OnPropertyChanged();
            }
        }

        public ObservableCollection<PhotoListModel> Photos { get; set; } = new ObservableCollection<PhotoListModel>();

        public ICommand SelectPhotoCommand { get; }
        public ICommand SearchPhotoCommand { get; }
        public ICommand AddPhotoSelected { get; }
        public ICommand OrderByNameCommand { get; set; }
        public ICommand OrderByDateCommand { get; set; }
        public ICommand FilterByFormatCommand { get; set; }
        public ICommand FilterByResolutionCommand { get; set; }
        public ICommand FilterByDateCommand { get; set; }
        public ICommand FindPhotoByNameCommand { get; set; }



        public PhotoListViewModel(PhotoRepository photoRepository, IMessenger messenger, PhotoMapper mapper)
        {
            ShowView = false;
            this.photoRepository = photoRepository;
            this.mapper = mapper; 
            this.messenger = messenger;
            this.messenger.Register<UpdatedPhotoMessage>(UpdatedPhotoMessageReceived);
            this.messenger.Register<DeletedPhotoMessage>(DeletedPhotoMessageReceived);
            this.messenger.Register<NewPhotoInAlbumMessage>(NewPhotoInAlbumMessageReceived);
            SelectPhotoCommand = new RelayCommand(PhotoSelectionChanged);

            SearchPhotoCommand = new RelayCommand(PhotoSearchChanged);
            AddPhotoSelected = new RelayCommand(AddPhotoToAlbum);
            OrderByNameCommand = new RelayCommand(OrderPhotosByName);
            OrderByDateCommand = new RelayCommand(OrderPhotosByDate);
            FilterByFormatCommand = new FilterByFormatCommand(this,photoRepository);
            FilterByResolutionCommand = new FilterByResolutionCommand(this, photoRepository);
            FilterByDateCommand = new FilterByDateCommand(this, photoRepository);
            FindPhotoByNameCommand = new FindPhotoByNameCommand(this,photoRepository);

        }


        private void OrderPhotosByDate()
        {
            var photos = photoRepository.OrderPhotosByDate();
            Photos.Clear();
            foreach (var p in photos)
            {
                Photos.Add(p);
            }
        }

        private void OrderPhotosByName()
        {
            var photos = photoRepository.OrderPhotosByName();
            Photos.Clear();
            foreach (var p in photos)
            {
                Photos.Add(p);
            }
        }

        private void NewPhotoInAlbumMessageReceived(NewPhotoInAlbumMessage message)
        {
            ShowView = true;
            OnLoad();
        }

        private void AddPhotoToAlbum(object listItem)
        {
            if(listItem is PhotoListModel photo)
            {
               
                this.messenger.Send(new AddPhotoToAlbumMessage(photo));
                ShowView = false;
            }
            
        }

        private void DeletedPhotoMessageReceived(DeletedPhotoMessage message)
        {
           var photoToDelete = Photos.SingleOrDefault(p => p.Id == message.photoId);
            Photos.Remove(photoToDelete);
        }

        private void UpdatedPhotoMessageReceived(UpdatedPhotoMessage message)
        {
            var photo = Photos.SingleOrDefault(a => a.Id == message.Model.Id);
            var photoList = mapper.MapDetailModelToListModel(message.Model);
            if (photo == null)
            {
                Photos.Add(photoList);
            }
            else
            {
                var photoIndex = Photos.IndexOf(photo);
                Photos[photoIndex] = photoList;
            }
        }

        public void OnLoad()
        {
            Photos.Clear();

            var photos = photoRepository.GetAll();
            foreach (var photo in photos)
            {
                Photos.Add(photo);
            }
        }

        private void PhotoSelectionChanged(object listItem)
        {
            if (listItem is PhotoListModel photo)
            {
                messenger.Send(new SelectedPhotoMessage { Id = photo.Id });
            }

        }

        private void PhotoSearchChanged(object photoName)
        {
            Photos.Clear();

            if (photoName is string s)
            {
                var photos = photoRepository.GetByName(s);

                foreach (var photo in photos)
                {
                    Photos.Add(photo);
                }
            }
        }
    }
}
