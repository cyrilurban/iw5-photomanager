using System;
using System.Collections.ObjectModel;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Input;

using PhotoManager.App.Commands;
using PhotoManager.BL;
using PhotoManager.BL.Messages;
using PhotoManager.BL.Models;
using PhotoManager.BL.Repositories;
using System.Linq;
using PhotoManager.BL.Mappers;

namespace PhotoManager.App.ViewModels
{
    public class ItemListViewModel : ViewModelBase
    {
        private readonly ItemRepository itemRepository;
        private readonly IMessenger messenger;
        private readonly ItemMapper mapper;
        private PhotoDetailModel photo;
        private string name;

        private bool showView;
        public bool ShowView
        {
            get { return showView; }
            set
            {
                showView = value;
                OnPropertyChanged();
            }
        }
        public string Name
        {
            get { return name; }
            set
            {
                name = value;
                OnPropertyChanged();
            }
        }

        public ObservableCollection<ItemListModel> Items { get; set; } = new ObservableCollection<ItemListModel>();

        public ICommand SelectItemCommand { get; }
        public ICommand SearchItemCommand { get; }
        public ICommand AddItemSelected { get; }

        public ICommand FindItemByNameCommand { get; set; }

        public ItemListViewModel(ItemRepository itemRepository, IMessenger messenger, ItemMapper mapper)
        {
            this.itemRepository = itemRepository;
            this.messenger = messenger;
            this.mapper = mapper;
            SelectItemCommand = new RelayCommand(ItemSelectionChanged);
            ShowView = false;
            SearchItemCommand = new RelayCommand(ItemSearchChanged);
            AddItemSelected = new RelayCommand(AddItemToPhoto);
            FindItemByNameCommand = new FindItemByNameCommand(this, itemRepository);
            this.messenger.Register<UpdatedItemMessage>(UpdatedItemMessageReceived);
            this.messenger.Register<DeletedItemMessage>(DeletedItemMessageReceived);
            this.messenger.Register<NewItemInPhotoMessage>(NewItemInPhotoMessageReceived);
        }

        private void NewItemInPhotoMessageReceived(NewItemInPhotoMessage message)
        {
            if(message.photo is PhotoDetailModel photoMessage)
            {
                photo = photoMessage; 
            }
            ShowView = true;
            OnLoad();
        }

        private void AddItemToPhoto(object listItem)
        {
            if(listItem is ItemListModel item)
            {
                var itemModel = itemRepository.GetById(item.Id);
                messenger.Send(new SetPositionOfItemInPhotoMessage(item,photo));
                ShowView = false;
            }
            
        }

        private void DeletedItemMessageReceived(DeletedItemMessage message)
        {
            var itemToDelete = Items.SingleOrDefault(i => i.Id == message.itemId);
            Items.Remove(itemToDelete);
        }

        private void UpdatedItemMessageReceived(UpdatedItemMessage message)
        {
            var item = Items.SingleOrDefault(a => a.Id == message.Model.Id);
            var itemList = mapper.MapDetailModelToListModel(message.Model);
            if (item == null)
            {
                Items.Add(itemList);
            }
            else
            {
                var itemIndex = Items.IndexOf(item);
                Items[itemIndex] = itemList;
            }
        }

        public void OnLoad()
        {
            Items.Clear();

            var items = itemRepository.GetAll();
            foreach (var item in items)
            {
                Items.Add(item);
            }
        }

        private void ItemSelectionChanged(object listItem)
        {
            if (listItem is ItemListModel item)
            {
                messenger.Send(new SelectedItemMessage { Id = item.Id });
            }

        }

        private void ItemSearchChanged(object itemName)
        {
            Items.Clear();
            
            if (itemName is string s)
            {
                var items = itemRepository.GetByName(s);
                
                foreach(var item in items)
                {
                    Items.Add(item);
                }
            }
        }
    }
}