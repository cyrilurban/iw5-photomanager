using System;
using System.Collections.ObjectModel;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Input;

using PhotoManager.App.Commands;
using PhotoManager.BL;
using PhotoManager.BL.Messages;
using PhotoManager.BL.Models;
using PhotoManager.BL.Repositories;
using System.Linq;
using PhotoManager.BL.Mappers;

namespace PhotoManager.App.ViewModels
{
    public class PersonListViewModel : ViewModelBase
    {
        private readonly PersonRepository personRepository;
        private readonly IMessenger messenger;
        private readonly PersonMapper mapper;
        private PhotoDetailModel photo;
        private string firstname;
        private string surname;
        private bool showView;
        public bool ShowView
        {
            get { return showView; }
            set
            {
                showView = value;
                OnPropertyChanged();
            }
        }
        public string Firstname
        {
            get { return firstname; }
            set
            {
                firstname = value;
                OnPropertyChanged();
            }
        }
        public string Surname
        {
            get { return surname; }
            set
            {
                surname = value;
                OnPropertyChanged();
            }
        }
        public ObservableCollection<PersonListModel> Persons { get; set; } = new ObservableCollection<PersonListModel>();

        public ICommand SelectPersonCommand { get; }
        public ICommand SearchPersonCommand { get; }
        public ICommand AddPersonSelected { get; }
        public ICommand FindPersonByNameCommand { get; set; }

        public PersonListViewModel(PersonRepository personRepository, IMessenger messenger, PersonMapper mapper)
        {
            this.personRepository = personRepository;
            this.mapper = mapper;
            this.messenger = messenger;
            this.messenger.Register<UpdatedPersonMessage>(UpdatedPersonMessageReceived);
            this.messenger.Register<DeletedPersonMessage>(DeletedPersonMessageReceived);
            this.messenger.Register<NewPersonInPhotoMessage>(NewPersonInPhotoMessageReceived);
            SelectPersonCommand = new RelayCommand(PersonSelectionChanged);
            AddPersonSelected = new RelayCommand(AddPersonToPhoto);
            SearchPersonCommand = new RelayCommand(PersonSearchChanged);
            FindPersonByNameCommand = new FindPersonByNameCommand(this, personRepository);
            ShowView = false;
        }

        private void NewPersonInPhotoMessageReceived(NewPersonInPhotoMessage message)
        {
            if(message.photo is PhotoDetailModel photoMessage)
            {
                photo = photoMessage;
            }
            ShowView = true;
            OnLoad();
        }

        private void AddPersonToPhoto(object listItem)
        {
            if (listItem is PersonListModel person)
            {
                var itemModel = personRepository.GetById(person.Id);
                messenger.Send(new SetPositionOfPersonInPhotoMessage(person, photo));
                ShowView = false;
            }
        }

        private void DeletedPersonMessageReceived(DeletedPersonMessage message)
        {
            var personToDelete = Persons.SingleOrDefault(p => p.Id == message.personId);
            Persons.Remove(personToDelete);
        }

        private void UpdatedPersonMessageReceived(UpdatedPersonMessage message)
        {
            var person = Persons.SingleOrDefault(a => a.Id == message.Model.Id);
            var personList = mapper.MapDetailModelToListModel(message.Model);
            if (person == null)
            {
                Persons.Add(personList);
            }
            else
            {
                var itemIndex = Persons.IndexOf(person);
                Persons[itemIndex] = personList;
            }
        }

        public void OnLoad()
        {
            Persons.Clear();

            var persons = personRepository.GetAll();
            foreach (var person in persons)
            {
                Persons.Add(person);
            }
        }

        private void PersonSelectionChanged(object listItem)
        {
            if (listItem is PersonListModel person)
            {
                messenger.Send(new SelectedPersonMessage { Id = person.Id });
            }

        }

        private void PersonSearchChanged(object personName)
        {
            Persons.Clear();

            if (personName is string s)
            {
                var persons = personRepository.GetByName(s);

                foreach (var person in persons)
                {
                    Persons.Add(person);
                }
            }
        }
    }
}