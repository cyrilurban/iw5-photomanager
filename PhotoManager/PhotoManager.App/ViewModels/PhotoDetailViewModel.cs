using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Input;
using PhotoManager.App.Commands;
using PhotoManager.BL;
using PhotoManager.BL.Messages;
using PhotoManager.BL.Models;
using PhotoManager.BL.Repositories;
using PhotoManager.DAL.Entities;
using PhotoManager.BL.Mappers;
using System.Collections.ObjectModel;
using Microsoft.Win32;
using System.Drawing;

namespace PhotoManager.App.ViewModels
{
    public class PhotoDetailViewModel : ViewModelBase
    {
        private readonly PhotoRepository photoRepository;
        private readonly PersonTagRepository personTagRepository;
        private readonly ItemTagRepository itemTagRepository;
        private readonly IMessenger messenger;
        private PhotoDetailModel detail;
        private bool showDetail;
        private ItemTagMapper itemMapper;
        private PersonTagMapper personMapper;
        private PersonTagListModel personTag;
        private ItemTagListModel itemTag;
        private string imagePath;

        public string ImagePath
        {
            get { return imagePath; }
            set
            {
                if (Equals(value, ImagePath)) return;

                imagePath = value;
                OnPropertyChanged();
            }
        }


        public PersonTagListModel SelectedPersonTag
        {
            get { return personTag; }
            set
            {
                if (Equals(value, SelectedPersonTag)) return;

                personTag = value;
                OnPropertyChanged();
            }
        }
        public ItemTagListModel SelectedItemTag
        {
            get { return itemTag; }
            set
            {
                if (Equals(value, SelectedItemTag)) return;

                itemTag = value;
                OnPropertyChanged();
            }
        }
        public PhotoDetailModel Detail
        {
            get { return detail; }
            set
            {
                if (Equals(value, Detail)) return;

                detail = value;
                OnPropertyChanged();
            }
        }

        public bool ShowDetail
        {
            get { return showDetail; }
            set
            {

                showDetail = value;
                OnPropertyChanged();
            }
        }

     //   public IList<ITagEntity> Tags => Enum.GetValues(typeof(ITagEntity)).Cast<ITagEntity>().ToList();


        public ICommand SavePhotoCommand { get; set; }
        public ICommand DeletePhotoCommand { get; set; }
        public ICommand AddItemToPhotoCommand { get; set; }
        public ICommand AddPersonToPhotoCommand { get; set; }
        public ICommand DeletePersonTagCommand { get; set; }
        public ICommand SelectedPersonTagCommand { get; set; }
        public ICommand SelectedItemTagCommand { get; set; }
        public ICommand DeleteItemTagCommand { get; set; }
        public ICommand SelectedDateCommand { get; set; }
        public ICommand LoadImageCommand { get; set; }

        public PhotoDetailViewModel(PhotoRepository photoRepository, IMessenger messenger, ItemTagMapper itemMapper, PersonTagMapper personMapper, PersonTagRepository personTagRepository, ItemTagRepository itemTagRepository)
        {
            this.photoRepository = photoRepository;
            this.messenger = messenger;
            this.itemMapper = itemMapper;
            this.personMapper = personMapper;
            this.personTagRepository = personTagRepository;
            this.itemTagRepository = itemTagRepository;
            ShowDetail = true;
            SavePhotoCommand = new SavePhotoCommand(photoRepository, this, messenger);
            DeletePhotoCommand = new RelayCommand(DeletePhoto);
            DeletePersonTagCommand = new DeletePersonTagCommand(this, messenger, personTagRepository, photoRepository);
            SelectedPersonTagCommand = new RelayCommand(PersonSelected);
            SelectedItemTagCommand = new RelayCommand(ItemSelected);
            AddItemToPhotoCommand = new AddItemToPhotoCommand(this,photoRepository,messenger);
            AddPersonToPhotoCommand = new AddPersonToPhotoCommand(this,photoRepository,messenger);
            LoadImageCommand = new RelayCommand(LoadImage);
            SelectedDateCommand = new RelayCommand(SelectedDate);

            this.messenger.Register<SelectedPhotoMessage>(SelectedPhoto);
            this.messenger.Register<NewPhotoMessage>(NewPhotoMessageReceived);
            this.messenger.Register<UpdatedItemTagMessage>(UpdatedItemTagMessageReceived);
            this.messenger.Register<UpdatedPersonTagMessage>(UpdatedPersonTagMessageReceived);
            this.messenger.Register<DeletedPersonTagMessage>(DeletedPersonTagMessageReceived);
            this.messenger.Register<DeletedItemTagMessage>(DeletedItemTagMessageReceived);

        }

        private void SelectedDate(object dateTime)
        {
            if(dateTime is DateTime date)
            {
                Detail.Time = date;
            }
            
        }

        private void LoadImage()
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            if (openFileDialog.ShowDialog() == true)
            {
                ImagePath = openFileDialog.FileName;
                Image image = Image.FromFile(ImagePath);
                Detail = new PhotoDetailModel
                {
                    ResolutionX = image.Width,
                    ResolutionY = image.Height,
                    Format = new ImageFormatConverter().ConvertToString(image.RawFormat),
                    ImagePath = ImagePath,
                    Id = Detail.Id,
                    Albums = Detail.Albums,
                    ItemTags = Detail.ItemTags,
                    PersonTags = Detail.PersonTags,
                    Name = Detail.Name,
                    Note = Detail.Note,
                    Time = Detail.Time
                };
             
            }
                
        }

        private void DeletedItemTagMessageReceived(DeletedItemTagMessage message)
        {
            Detail = photoRepository.GetById(Detail.Id);
            ShowDetail = true;
        }

        private void ItemSelected(object listItem)
        {
            if (listItem is ItemTagListModel itemTag)
            {
                SelectedItemTag = itemTag;
                ShowDetail = false;
                messenger.Send(new ItemTagSelectedMessage(itemTag));
            }
        }

        private void DeletedPersonTagMessageReceived(DeletedPersonTagMessage message)
        {
            Detail = photoRepository.GetById(Detail.Id);
            ShowDetail = true;
        }

        private void PersonSelected(object listItem)
        {
            if (listItem is PersonTagListModel personTag)
            {
                SelectedPersonTag = personTag;
                ShowDetail = false;
                messenger.Send(new PersonTagSelectedMessage(personTag));
            }
        }

        private void UpdatedPersonTagMessageReceived(UpdatedPersonTagMessage message)
        {
            if (message.personTag is PersonTagDetailModel personTag)
            {
                ShowDetail = true;
             //   Detail.PersonTags.Add(personMapper.MapDetailModelToEntity(personTag));
                Detail = photoRepository.GetById(Detail.Id);
                messenger.Send(new UpdatedPhotoMessage(Detail));


            }
        }

        private void UpdatedItemTagMessageReceived(UpdatedItemTagMessage message)
        {
            if(message.itemTag is ItemTagDetailModel itemTag)
            {
                ShowDetail = true;
                Detail = photoRepository.GetById(Detail.Id);
                messenger.Send(new UpdatedPhotoMessage(Detail));
                

            }
        }



        private void DeletePhoto()
        {
            if (Detail.Id != Guid.Empty)
            {
                var photoId = Detail.Id;

                Detail = new PhotoDetailModel();
                photoRepository.Remove(photoId);
                messenger.Send(new DeletedPhotoMessage(photoId));
            }
        }

        private void NewPhotoMessageReceived(NewPhotoMessage message)
        {
            Detail = new PhotoDetailModel();
        }

        private void SelectedPhoto(SelectedPhotoMessage message)
        {
            Detail = photoRepository.GetById(message.Id);
            if(Detail.ItemTags == null)
            {
                Detail.ItemTags = new List<ItemTagListModel>();
            }
            if (Detail.PersonTags == null)
            {
                Detail.PersonTags = new List<PersonTagListModel>();
            }
        }
    }
}