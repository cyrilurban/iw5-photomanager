using System.Windows.Input;

using PhotoManager.BL;

using PhotoManager.App.Commands;
using PhotoManager.BL.Messages;
using System;

namespace PhotoManager.App.ViewModels
{
    public class MainViewModel : ViewModelBase
    {
        private readonly IMessenger messenger;


        public string Name { get; set; } = "Nenacteno";
        public ICommand CreateAlbumCommand { get; set; }
        public ICommand CreatePhotoCommand { get; set; }
        public ICommand CreateItemCommand { get; set; }
        public ICommand CreatePersonCommand { get; set; }


        public MainViewModel(IMessenger messenger)
        {
            this.messenger = messenger;
            CreateAlbumCommand = new RelayCommand(AddNewAlbum);
            CreatePhotoCommand = new RelayCommand(AddNewPhoto);
            CreateItemCommand = new RelayCommand(AddNewItem);
            CreatePersonCommand = new RelayCommand(AddNewPerson);
            
        }

       

        private void AddNewPerson()
        {
            messenger.Send(new NewPersonMessage());
        }

        private void AddNewItem()
        {
            messenger.Send(new NewItemMessage());
        }



        private void AddNewPhoto()
        {
            messenger.Send(new NewPhotoMessage());
        }

        private void AddNewAlbum()
        {
            messenger.Send(new NewAlbumMessage());
        }

        public void OnLoad()
        {
        }
    }
}