﻿using PhotoManager.App.Commands;
using PhotoManager.BL;
using PhotoManager.BL.Mappers;
using PhotoManager.BL.Messages;
using PhotoManager.BL.Models;
using PhotoManager.BL.Repositories;
using PhotoManager.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace PhotoManager.App.ViewModels
{
    public class PersonTagViewModel : ViewModelBase
    {
        private readonly PersonTagRepository personTagRepository;
        private readonly IMessenger messenger;
        private PersonTagDetailModel detail;
        private readonly PersonRepository personRepository;
        private readonly PersonMapper personMapper;
        private readonly PhotoMapper photoMapper;
        private bool showView;
        private BitmapImage imageBrush;
        private bool showUpdateView;
        public bool ShowView
        {
            get { return showView; }
            set
            {
                showView = value;
                OnPropertyChanged();
            }
        }

        public bool ShowUpdateView
        {
            get { return showUpdateView; }
            set
            {
                showUpdateView = value;
                OnPropertyChanged();
            }
        }

        public BitmapImage ImageBrush
        {
            get { return imageBrush; }
            set
            {
                if (Equals(value, ImageBrush)) return;
                imageBrush = value;
                OnPropertyChanged();
            }
        }

        public PersonTagDetailModel Detail
        {
            get { return detail; }
            set
            {
                if (Equals(value, Detail)) return;

                detail = value;
                OnPropertyChanged();
            }
        }

        //       public ObservableCollection<PhotoEntity> Photos { get; set; } = new ObservableCollection<PhotoEntity>();
        //       public ObservableCollection<ItemEntity> Items { get; set; } = new ObservableCollection<ItemEntity>();

        public ICommand SetPositionCommand { get; set; }
        public ICommand MouseDown { get; set; }
        public ICommand DeletePersonTagCommand { get; set; }
        public ICommand UpdatePersonTagCommand { get; set; }

        public PhotoMapper PhotoMapper => photoMapper;

        public PersonTagViewModel(PersonTagRepository personTagRepository, IMessenger messenger, PersonRepository personRepository, PersonMapper personMapper, PhotoMapper photoMapper)
        {
            this.personTagRepository = personTagRepository;
            this.personRepository = personRepository;
            this.personMapper = personMapper;
            this.photoMapper = photoMapper;
            this.messenger = messenger;
            this.messenger.Register<SetPositionOfPersonInPhotoMessage>(SetPositionsReceived);
            this.messenger.Register<PersonTagSelectedMessage>(PersonTagSelectedMessageReceived);
            SetPositionCommand = new RelayCommand(SetPosition);
            UpdatePersonTagCommand = new RelayCommand(UpdatePersonTag);
            DeletePersonTagCommand = new RelayCommand(DeletePersonTag);
            MouseDown = new RelayCommand(OnMouseClick);
            ShowView = false;
            ShowUpdateView = false;
        }

        private void OnMouseClick(object obj)
        {
            if (obj is Canvas canvas)
            {
                canvas.Children.Clear();
                var coordinates = Mouse.GetPosition(canvas);
                Detail = new PersonTagDetailModel
                {
                    Id = Detail.Id,
                    Photo = Detail.Photo,
                    Person = Detail.Person,
                    PositionX = (int)coordinates.X,
                    PositionY = (int)coordinates.Y
                };
                Ellipse ellipse = new Ellipse();
                ellipse.Width = 20;
                ellipse.Height = 20;
                ellipse.Fill = System.Windows.Media.Brushes.Black;
                ellipse.Margin = new System.Windows.Thickness(Detail.PositionX - 10, Detail.PositionY - 10, 0, 0);
                canvas.Children.Add(ellipse);
            }
        }

        private void DeletePersonTag(object obj)
        {
            ShowUpdateView = false;
            personTagRepository.Remove(Detail.Id);
            messenger.Send(new DeletedPersonTagMessage(Detail.Id));
        }

        private void UpdatePersonTag(object obj)
        {
            var detailInDB = personTagRepository.GetById(Detail.Id);
            detailInDB.PositionX = Detail.PositionX;
            detailInDB.PositionY = Detail.PositionY;
            Detail = personTagRepository.Update(detailInDB);
            ShowUpdateView = false;
            messenger.Send(new UpdatedPersonTagMessage(Detail));
        }

        private void PersonTagSelectedMessageReceived(PersonTagSelectedMessage message)
        {
            if(message.detail is PersonTagListModel entity)
            {
                ShowUpdateView = true;
                Detail = personTagRepository.GetById(entity.Id);
                ImageBrush = new BitmapImage(new Uri(Detail.Photo.ImagePath, UriKind.Absolute));


            }
        }

        private void SetPosition()
        {
            ShowView = false;
            Detail = personTagRepository.Insert(Detail);
            messenger.Send(new UpdatedPersonTagMessage(Detail));
        }

        private void SetPositionsReceived(SetPositionOfPersonInPhotoMessage message)
        {
            if (message.person is PersonListModel person && message.photo is PhotoDetailModel photo)
            {
                Detail = new PersonTagDetailModel();
                Detail.Person = personRepository.GetById(person.Id);
                Detail.Photo = photo;
                ImageBrush = new BitmapImage(new Uri(Detail.Photo.ImagePath, UriKind.Absolute));
                ShowView = true;
            }

        }
    }
}
