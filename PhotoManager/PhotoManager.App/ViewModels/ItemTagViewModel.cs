﻿using PhotoManager.App.Commands;
using PhotoManager.BL;
using PhotoManager.BL.Mappers;
using PhotoManager.BL.Messages;
using PhotoManager.BL.Models;
using PhotoManager.BL.Repositories;
using PhotoManager.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace PhotoManager.App.ViewModels
{
    public class ItemTagViewModel : ViewModelBase
    {
        private readonly ItemTagRepository itemTagRepository;
        private readonly IMessenger messenger;
        private ItemTagDetailModel detail;
        private readonly ItemRepository itemRepository;
        private readonly ItemMapper itemMapper;
        private readonly PhotoMapper photoMapper;
        private readonly ItemTagMapper itemTagMapper;
        private bool showView;
        private bool showUpdateView;
        private BitmapImage imageBrush;
        public bool ShowView
        {
            get { return showView; }
            set
            {
                showView = value;
                OnPropertyChanged();
            }
        }

        public bool ShowUpdateView
        {
            get { return showUpdateView; }
            set
            {
                showUpdateView = value;
                OnPropertyChanged();
            }
        }

        public BitmapImage ImageBrush
        {
            get { return imageBrush; }
            set
            {
                if (Equals(value, ImageBrush)) return;
                imageBrush = value;
                OnPropertyChanged();
            }
        }

        public ItemTagDetailModel Detail
        {
            get { return detail; }
            set
            {
                if (Equals(value, Detail)) return;

                detail = value;
                OnPropertyChanged();
            }
        }

        public ICommand SetPositionCommand { get; set; }
        public ICommand MouseDown { get; set; }
        public ICommand DeleteItemTagCommand { get; set; }
        public ICommand UpdateItemTagCommand { get; set; }


        public ItemTagViewModel(ItemTagRepository itemTagRepository, IMessenger messenger, ItemRepository itemRepository, ItemMapper itemMapper, PhotoMapper photoMapper, ItemTagMapper itemTagMapper)
        {
            this.itemTagRepository = itemTagRepository;
            this.itemRepository = itemRepository;
            this.itemMapper = itemMapper;
            this.photoMapper = photoMapper;
            this.messenger = messenger;
            this.itemTagMapper = itemTagMapper;
            this.messenger.Register<SetPositionOfItemInPhotoMessage>(SetPositionsReceived);
            this.messenger.Register<ItemTagSelectedMessage>(ItemTagSelectedMessageReceived);
            SetPositionCommand = new RelayCommand(SetPosition);
            UpdateItemTagCommand = new RelayCommand(UpdateItemTag);
            DeleteItemTagCommand = new RelayCommand(DeleteItemTag);
            MouseDown = new RelayCommand(OnMouseClick);
            ShowView = false;
            ShowUpdateView = false;

        }


        private void DeleteItemTag()
        {
            ShowUpdateView = false;
            itemTagRepository.Remove(Detail.Id);
            messenger.Send(new DeletedItemTagMessage(Detail.Id));
        }

        private void UpdateItemTag()
        {
            var detailInDB = itemTagRepository.GetById(Detail.Id);
            detailInDB.PositionX = Detail.PositionX;
            detailInDB.PositionY = Detail.PositionY;
            Detail = itemTagRepository.Update(detailInDB);
            ShowUpdateView = false;
            messenger.Send(new UpdatedItemTagMessage(Detail));

        }

        private void ItemTagSelectedMessageReceived(ItemTagSelectedMessage message)
        {
            if(message.detail is ItemTagListModel entity)
            {
                ShowUpdateView = true;
                Detail = itemTagRepository.GetById(entity.Id);
                ImageBrush = new BitmapImage(new Uri(Detail.Photo.ImagePath, UriKind.Absolute));

                
            }
        }

        private void OnMouseClick(object Canvas)
        {
            if(Canvas is Canvas canvas)
            {
                canvas.Children.Clear();
                var coordinates = Mouse.GetPosition(canvas);
                Detail = new ItemTagDetailModel
                {
                    Id = Detail.Id,
                    Photo = Detail.Photo,
                    Item = Detail.Item,
                    PositionX =(int) coordinates.X,
                    PositionY =(int) coordinates.Y
                };
                Ellipse ellipse = new Ellipse();
                ellipse.Width = 20;
                ellipse.Height = 20;
                ellipse.Fill = System.Windows.Media.Brushes.Black;
                ellipse.Margin = new System.Windows.Thickness(Detail.PositionX-10, Detail.PositionY-10, 0, 0);
                canvas.Children.Add(ellipse);
            }
        }

        private void SetPosition()
        {
            ShowView = false;
            Detail = itemTagRepository.Insert(Detail);
            messenger.Send(new UpdatedItemTagMessage(Detail));
        }

        private void SetPositionsReceived(SetPositionOfItemInPhotoMessage message)
        {
            if(message.item is ItemListModel item && message.photo is PhotoDetailModel photo)
            {
                Detail = new ItemTagDetailModel();
                Detail.Item = itemRepository.GetById(item.Id);
                Detail.Photo = photo;
                ImageBrush = new BitmapImage(new Uri(Detail.Photo.ImagePath, UriKind.Absolute));
                ShowView = true;
            }
            
        }
    }
}
