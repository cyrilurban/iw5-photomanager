using System;
using System.Collections.ObjectModel;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Input;

using PhotoManager.App.Commands;
using PhotoManager.BL;
using PhotoManager.BL.Messages;
using PhotoManager.BL.Models;
using PhotoManager.BL.Repositories;
using System.Linq;
using PhotoManager.BL.Mappers;

namespace PhotoManager.App.ViewModels
{
    public class AlbumListViewModel : ViewModelBase
    {
        private readonly AlbumRepository albumRepository;
        private readonly IMessenger messenger;
        private readonly AlbumMapper mapper;

        public ObservableCollection<AlbumListModel> Albums { get; set; } = new ObservableCollection<AlbumListModel>();

        public ICommand SelectAlbumCommand { get; }

        public AlbumListViewModel(AlbumRepository albumRepository, IMessenger messenger, AlbumMapper mapper)
        {
            this.albumRepository = albumRepository;
            this.messenger = messenger;
            this.mapper = mapper;
            this.messenger.Register<UpdatedAlbumMessage>(UpdateAlbumList);
            this.messenger.Register<DeletedAlbumMessage>(DeletedAlbumMessageReceived);
            SelectAlbumCommand = new RelayCommand(AlbumSelectionChanged);
        }

        private void DeletedAlbumMessageReceived(DeletedAlbumMessage message)
        {
            var albumToRemove = Albums.SingleOrDefault(a => a.Id == message.albumId);
            Albums.Remove(albumToRemove);
        }

        private void UpdateAlbumList(UpdatedAlbumMessage message)
        {
            var album = Albums.SingleOrDefault(a => a.Id == message.Model.Id);
            var albumList = mapper.MapDetailModelToListModel(message.Model);
            if (album == null)
            {
                Albums.Add(albumList);
            }
            else
            {
                var albumIndex = Albums.IndexOf(album);
                Albums[albumIndex] = albumList;
            }
        }

        private void AddNewPhoto()
        {
            messenger.Send(new NewPhotoMessage());
        }

        public void OnLoad()
        {
            Albums.Clear();

            var albums = albumRepository.GetAll();
            foreach (var album in albums)
            {
                Albums.Add(album);
            }
        }

        private void AlbumSelectionChanged(object listItem)
        {
            if (listItem is AlbumListModel album)
            {
                messenger.Send(new SelectedAlbumMessage { Id = album.Id });
            }

        }
    }
}