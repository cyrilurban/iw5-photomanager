﻿using System;
using System.Collections.ObjectModel;
using System.Windows.Input;
using PhotoManager.App.Commands;
using PhotoManager.BL;
using PhotoManager.BL.Messages;
using PhotoManager.BL.Models;
using PhotoManager.BL.Repositories;
using PhotoManager.DAL.Entities;

namespace PhotoManager.App.ViewModels
{
    public class AlbumDetailViewModel : ViewModelBase
    {
        private readonly AlbumRepository albumRepository;
        private readonly IMessenger messenger;
        private AlbumDetailModel detail;
        private bool showDetail;

        public ICommand SelectPhotoCommand { get; }

        public AlbumDetailModel Detail
        {
            get { return detail; }
            set
            {
                if (Equals(value, Detail)) return;

                detail = value;
                OnPropertyChanged();
            }
        }

        public bool ShowDetail
        {
            get { return showDetail; }
            set
            {

                showDetail = value;
                OnPropertyChanged();
            }
        }
        
        public ICommand SaveAlbumCommand { get; set; }
        public ICommand DeleteAlbumCommand { get; set; }
        public ICommand AddPhotoToAlbumCommand { get; set; }

        public AlbumDetailViewModel(AlbumRepository albumRepository, IMessenger messenger)
        {
            ShowDetail = true;
            this.albumRepository = albumRepository;
            this.messenger = messenger;

            SaveAlbumCommand = new SaveAlbumCommand(albumRepository, this, messenger);
            DeleteAlbumCommand = new RelayCommand(DeleteAlbum);
            AddPhotoToAlbumCommand = new AddPhotoToAlbumCommand(this,albumRepository,messenger);
            SelectPhotoCommand = new RelayCommand(PhotoSelectionChanged);

            this.messenger.Register<SelectedAlbumMessage>(SelectedAlbum);
            this.messenger.Register<NewAlbumMessage>(NewAlbumMessageReceived);
            this.messenger.Register<AddPhotoToAlbumMessage>(AddPhotoToAlbumReceived);
        }

 /*       private void AddPhotoToAlbum(object obj)
        {
            ShowDetail = false;
            messenger.Send(new NewPhotoInAlbumMessage());
            
        }*/

        private void AddPhotoToAlbumReceived(AddPhotoToAlbumMessage message)
        {
            ShowDetail = true;
            Detail.Photos.Add(message.photo);
            Detail = albumRepository.Update(Detail);
            messenger.Send(new UpdatedAlbumMessage(Detail));
        }

        public void OnLoad()
        {
        }

        public void PhotoSelectionChanged(object listItem)
        {
            if (listItem is PhotoListModel photo)
            {
                messenger.Send(new SelectedPhotoMessage { Id = photo.Id });
            }

        }

        private void DeleteAlbum()
        {
            if (Detail.Id != Guid.Empty)
            {
                var albumId = Detail.Id;

                Detail = new AlbumDetailModel();
                albumRepository.Remove(albumId);
                messenger.Send(new DeletedAlbumMessage(albumId));
            }
        }

        private void NewAlbumMessageReceived(NewAlbumMessage message)
        {
            Detail = new AlbumDetailModel();
        }

        private void SelectedAlbum(SelectedAlbumMessage message)
        {
            Detail = albumRepository.GetById(message.Id);

        }
    }
}