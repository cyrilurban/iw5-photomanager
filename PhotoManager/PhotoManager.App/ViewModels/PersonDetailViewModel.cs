﻿using System;
using System.Collections.ObjectModel;
using System.Windows.Input;
using PhotoManager.App.Commands;
using PhotoManager.BL;
using PhotoManager.BL.Messages;
using PhotoManager.BL.Models;
using PhotoManager.BL.Repositories;
using PhotoManager.DAL.Entities;

namespace PhotoManager.App.ViewModels
{
    public class PersonDetailViewModel : ViewModelBase
    {
        private readonly PersonRepository personRepository;
        private readonly IMessenger messenger;
        private PersonDetailModel detail;

        public ICommand SelectPhotoCommand { get; }

        public PersonDetailModel Detail
        {
            get { return detail; }
            set
            {
                if (Equals(value, Detail)) return;

                detail = value;
                OnPropertyChanged();
            }
        }

        public ICommand SavePersonCommand { get; set; }
        public ICommand DeletePersonCommand { get; set; }

        public PersonDetailViewModel(PersonRepository personRepository, IMessenger messenger)
        {
            this.personRepository = personRepository;
            this.messenger = messenger;

            SavePersonCommand = new SavePersonCommand(personRepository, this, messenger);
            DeletePersonCommand = new RelayCommand(DeletePerson);
            SelectPhotoCommand = new RelayCommand(PhotoSelectionChanged);

            this.messenger.Register<SelectedPersonMessage>(SelectedPerson);
            this.messenger.Register<NewPersonMessage>(NewPersonMessageReceived);
            this.messenger.Register<UpdatedPersonTagMessage>(UpdatedPersonTagMessageReceived);
            this.messenger.Register<DeletedPersonTagMessage>(DeletedPersonTagMessageReceived);
        }

        private void DeletedPersonTagMessageReceived(DeletedPersonTagMessage message)
        {
            Detail = personRepository.GetById(message.personId);
        }

        private void UpdatedPersonTagMessageReceived(UpdatedPersonTagMessage message)
        {
            Detail = personRepository.GetById(message.personTag.Photo.Id);
        }

        public void OnLoad()
        {
        }

        public void PhotoSelectionChanged(object listItem)
        {
            if (listItem is PhotoListModel photo)
            {
                messenger.Send(new SelectedPhotoMessage { Id = photo.Id });
            }

        }

        private void DeletePerson()
        {
            if (Detail.Id != Guid.Empty)
            {
                var personId = Detail.Id;

                Detail = new PersonDetailModel();
                personRepository.Remove(personId);
                messenger.Send(new DeletedPersonMessage(personId));
            }
        }

        private void NewPersonMessageReceived(NewPersonMessage message)
        {
            Detail = new PersonDetailModel();
        }

        private void SelectedPerson(SelectedPersonMessage message)
        {
            Detail = personRepository.GetById(message.Id);

        }
    }
}