﻿using PhotoManager.App.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace PhotoManager.App.Views
{
    /// <summary>
    /// Interaction logic for ItemListView.xaml
    /// </summary>
    public partial class ItemListView : UserControl
    {
        public ItemListView()
        {
            InitializeComponent();
            Loaded += ItemListViewLoaded;
        }

        private void ItemListViewLoaded(object sender, System.Windows.RoutedEventArgs e)
        {
            if (DataContext is ItemListViewModel viewModel)
            {
                viewModel.OnLoad();
            }
        }
    }
}
