﻿using PhotoManager.App.ViewModels;
using PhotoManager.BL;
using PhotoManager.BL.Mappers;
using PhotoManager.BL.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhotoManager.App
{
    public class ViewModelLocator
    {
        private readonly Messenger messenger = new Messenger();
        private readonly AlbumRepository albumRepository = new AlbumRepository();
        private readonly ItemRepository itemRepository = new ItemRepository();
        private readonly ItemTagRepository itemTagRepository = new ItemTagRepository();
        private readonly PersonRepository personRepository = new PersonRepository();
        private readonly PersonTagRepository personTagRepository = new PersonTagRepository();
        private readonly PhotoRepository photoRepository = new PhotoRepository();

        private readonly AlbumMapper albumMapper = new AlbumMapper();
        private readonly ItemMapper itemMapper = new ItemMapper();
        private readonly PersonMapper personMapper = new PersonMapper();
        private readonly PhotoMapper photoMapper = new PhotoMapper();
        private readonly ItemTagMapper itemTagMapper = new ItemTagMapper();
        private readonly PersonTagMapper personTagMapper = new PersonTagMapper();

        public AlbumDetailViewModel AlbumDetailViewModel => CreateAlbumDetailViewModel();
        public AlbumListViewModel AlbumListViewModel => CreateAlbumListModel();
        public ItemDetailViewModel ItemDetailViewModel => CreateItemDetailViewModel();
        public ItemListViewModel ItemListViewModel => CreateItemListViewModel();
        public MainViewModel MainViewModel => CreateMainViewModel();
        public PersonDetailViewModel PersonDetailViewModel => CreatePersonDetailViewModel();
        public PersonListViewModel PersonListViewModel => CreatePersonListViewModel();
        public PhotoDetailViewModel PhotoDetailViewModel => CreatePhotoDetailViewModel();
        public PhotoListViewModel PhotoListViewModel => CreatePhotoListViewModel();
        public ItemTagViewModel ItemTagViewModel => CreateItemTagViewModel();
        public PersonTagViewModel PersonTagViewModel => CreatePersonTagViewModel();

        private PersonTagViewModel CreatePersonTagViewModel()
        {
            return new PersonTagViewModel(personTagRepository, messenger, personRepository, personMapper, photoMapper);
        }

        private ItemTagViewModel CreateItemTagViewModel()
        {
            return new ItemTagViewModel(itemTagRepository, messenger, itemRepository, itemMapper, photoMapper, itemTagMapper);
        }

        private PhotoDetailViewModel CreatePhotoDetailViewModel()
        {
            return new PhotoDetailViewModel(photoRepository, messenger, itemTagMapper, personTagMapper, personTagRepository, itemTagRepository);
        }

        private PhotoListViewModel CreatePhotoListViewModel()
        {
            return new PhotoListViewModel(photoRepository, messenger, photoMapper);
        }

        private PersonListViewModel CreatePersonListViewModel()
        {
            return new PersonListViewModel(personRepository, messenger, personMapper);
        }

        private PersonDetailViewModel CreatePersonDetailViewModel()
        {
            return new PersonDetailViewModel(personRepository, messenger);
        }

        private MainViewModel CreateMainViewModel()
        {
            return new MainViewModel(messenger);
        }

        private ItemListViewModel CreateItemListViewModel()
        {
            return new ItemListViewModel(itemRepository, messenger, itemMapper);
        }

        private ItemDetailViewModel CreateItemDetailViewModel()
        {
            return new ItemDetailViewModel(itemRepository, messenger, itemTagMapper);
        }

        private AlbumListViewModel CreateAlbumListModel()
        {
            return new AlbumListViewModel(albumRepository, messenger, albumMapper);
        }

        private AlbumDetailViewModel CreateAlbumDetailViewModel()
        {
            return new AlbumDetailViewModel(albumRepository, messenger);
        }

    }
}
